def __init__(hub):
    # This enables acct profiles that begin with "vra" for states
    hub.states.vra.ACCT = ["vra"]
