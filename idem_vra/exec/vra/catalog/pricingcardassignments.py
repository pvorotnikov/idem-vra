from idem_vra.client.vra_catalog_lib.api import PricingCardAssignmentsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def change_metering_assignment_strategy(hub, ctx, **kwargs):
    """Updates the pricing card assignment strategy for the Org.  Performs PATCH /price/api/private/pricing-card-assignments/strategy


    :param string entityType: (optional in body) Metering Policy Assignment entityType(Strategy)
    """

    try:

        hub.log.debug("PATCH /price/api/private/pricing-card-assignments/strategy")

        api = PricingCardAssignmentsApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        body = {}

        if "entityType" in kwargs:
            hub.log.debug(f"Got kwarg 'entityType' = {kwargs['entityType']}")
            body["entityType"] = kwargs.get("entityType")
            del kwargs["entityType"]

        ret = api.change_metering_assignment_strategy(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PricingCardAssignmentsApi.change_metering_assignment_strategy: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_metering_assignment_strategy(hub, ctx, **kwargs):
    """Selecting the new pricing card assignment strategy,PROJECT or CLOUDZONE are
      possible values can be used while creating strategy. Also there can be only one
      strategy for a given org at a given point of time Create a new pricing card assignment strategy based on request body and
      validate its field according to business rules. Performs POST /price/api/private/pricing-card-assignments/strategy


    :param string entityType: (optional in body) Metering Policy Assignment entityType(Strategy)
    """

    try:

        hub.log.debug("POST /price/api/private/pricing-card-assignments/strategy")

        api = PricingCardAssignmentsApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        body = {}

        if "entityType" in kwargs:
            hub.log.debug(f"Got kwarg 'entityType' = {kwargs['entityType']}")
            body["entityType"] = kwargs.get("entityType")
            del kwargs["entityType"]

        ret = api.create_metering_assignment_strategy(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PricingCardAssignmentsApi.create_metering_assignment_strategy: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_metering_policy_assignment(hub, ctx, pricingCardId, **kwargs):
    """Create a new pricing card assignment Create a new pricing card policy assignment based on request body and validate
      its field according to business rules. Request body with ALL entityType will
      delete the older assignments for the given pricingCardId Performs POST /price/api/private/pricing-card-assignments


    :param string pricingCardId: (required in body) Pricing card id
    :param string id: (optional in body) Id of the pricingCardAssignment
    :param string orgId: (optional in body) OrgId of the pricingCardAssignment
    :param string entityId: (optional in body) Pricing card assigned entity id
    :param string entityType: (optional in body) Pricing card assigned entity type
    :param string createdAt: (optional in body) Creation time
    :param string lastUpdatedAt: (optional in body) Updated time
    :param string entityName: (optional in body) Pricing card assigned entity name
    :param string pricingCardName: (optional in body) Pricing card name
    """

    try:

        hub.log.debug("POST /price/api/private/pricing-card-assignments")

        api = PricingCardAssignmentsApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        body = {}
        body["pricingCardId"] = pricingCardId

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "entityId" in kwargs:
            hub.log.debug(f"Got kwarg 'entityId' = {kwargs['entityId']}")
            body["entityId"] = kwargs.get("entityId")
            del kwargs["entityId"]
        if "entityType" in kwargs:
            hub.log.debug(f"Got kwarg 'entityType' = {kwargs['entityType']}")
            body["entityType"] = kwargs.get("entityType")
            del kwargs["entityType"]
        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "lastUpdatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'lastUpdatedAt' = {kwargs['lastUpdatedAt']}")
            body["lastUpdatedAt"] = kwargs.get("lastUpdatedAt")
            del kwargs["lastUpdatedAt"]
        if "entityName" in kwargs:
            hub.log.debug(f"Got kwarg 'entityName' = {kwargs['entityName']}")
            body["entityName"] = kwargs.get("entityName")
            del kwargs["entityName"]
        if "pricingCardName" in kwargs:
            hub.log.debug(f"Got kwarg 'pricingCardName' = {kwargs['pricingCardName']}")
            body["pricingCardName"] = kwargs.get("pricingCardName")
            del kwargs["pricingCardName"]

        ret = api.create_metering_policy_assignment(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PricingCardAssignmentsApi.create_metering_policy_assignment: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_metering_policy_assignment(hub, ctx, p_id, **kwargs):
    """Delete the pricing card assignment with specified id Deletes the pricing card assignment with the specified id Performs DELETE /price/api/private/pricing-card-assignments/{id}


    :param string p_id: (required in path) pricing card Assignment Id
    """

    try:

        hub.log.debug("DELETE /price/api/private/pricing-card-assignments/{id}")

        api = PricingCardAssignmentsApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        ret = api.delete_metering_policy_assignment(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PricingCardAssignmentsApi.delete_metering_policy_assignment: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_metering_policy_assignments(hub, ctx, **kwargs):
    """Fetch all pricing card assignment for private cloud Returns a paginated list of pricing card assignments Performs GET /price/api/private/pricing-card-assignments


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param boolean refreshEntities: (optional in query) Search by name and description
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /price/api/private/pricing-card-assignments")

        api = PricingCardAssignmentsApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        ret = api.get_all_metering_policy_assignments(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PricingCardAssignmentsApi.get_all_metering_policy_assignments: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_metering_assignment_strategy(hub, ctx, **kwargs):
    """Fetch pricing card assignment strategy for the Org Returns a pricing card assignment strategy for the Org Performs GET /price/api/private/pricing-card-assignments/strategy"""

    try:

        hub.log.debug("GET /price/api/private/pricing-card-assignments/strategy")

        api = PricingCardAssignmentsApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        ret = api.get_metering_assignment_strategy(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PricingCardAssignmentsApi.get_metering_assignment_strategy: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_metering_policy_assignment(hub, ctx, p_id, **kwargs):
    """Fetch pricing card assignment for private cloud by id Returns a pricing card assignments by id Performs GET /price/api/private/pricing-card-assignments/{id}


    :param string p_id: (required in path) pricing card assignment id
    """

    try:

        hub.log.debug("GET /price/api/private/pricing-card-assignments/{id}")

        api = PricingCardAssignmentsApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        ret = api.get_metering_policy_assignment(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PricingCardAssignmentsApi.get_metering_policy_assignment: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_metering_policy_assignment(hub, ctx, p_id, pricingCardId, **kwargs):
    """Updates the pricing card assignment id with the supplied id. Request body with
      ALL entityType will delete the older assignments for the given pricingCardId  Performs PATCH /price/api/private/pricing-card-assignments/{id}


    :param string p_id: (required in path) pricing card Assignment Id
    :param string pricingCardId: (required in body) Pricing card id
    :param string id: (optional in body) Id of the pricingCardAssignment
    :param string orgId: (optional in body) OrgId of the pricingCardAssignment
    :param string entityId: (optional in body) Pricing card assigned entity id
    :param string entityType: (optional in body) Pricing card assigned entity type
    :param string createdAt: (optional in body) Creation time
    :param string lastUpdatedAt: (optional in body) Updated time
    :param string entityName: (optional in body) Pricing card assigned entity name
    :param string pricingCardName: (optional in body) Pricing card name
    """

    try:

        hub.log.debug("PATCH /price/api/private/pricing-card-assignments/{id}")

        api = PricingCardAssignmentsApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        body = {}
        body["pricingCardId"] = pricingCardId

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "entityId" in kwargs:
            hub.log.debug(f"Got kwarg 'entityId' = {kwargs['entityId']}")
            body["entityId"] = kwargs.get("entityId")
            del kwargs["entityId"]
        if "entityType" in kwargs:
            hub.log.debug(f"Got kwarg 'entityType' = {kwargs['entityType']}")
            body["entityType"] = kwargs.get("entityType")
            del kwargs["entityType"]
        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "lastUpdatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'lastUpdatedAt' = {kwargs['lastUpdatedAt']}")
            body["lastUpdatedAt"] = kwargs.get("lastUpdatedAt")
            del kwargs["lastUpdatedAt"]
        if "entityName" in kwargs:
            hub.log.debug(f"Got kwarg 'entityName' = {kwargs['entityName']}")
            body["entityName"] = kwargs.get("entityName")
            del kwargs["entityName"]
        if "pricingCardName" in kwargs:
            hub.log.debug(f"Got kwarg 'pricingCardName' = {kwargs['pricingCardName']}")
            body["pricingCardName"] = kwargs.get("pricingCardName")
            del kwargs["pricingCardName"]

        ret = api.patch_metering_policy_assignment(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PricingCardAssignmentsApi.patch_metering_policy_assignment: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
