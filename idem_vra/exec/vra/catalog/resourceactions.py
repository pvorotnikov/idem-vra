from idem_vra.client.vra_catalog_lib.api import ResourceActionsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_resource_action(hub, ctx, p_resourceId, p_actionId, **kwargs):
    """Fetch resource action. Returns an action for the resource specified by its Resource ID and Action ID. Performs GET /deployment/api/resources/{resourceId}/actions/{actionId}


    :param string p_resourceId: (required in path) Resource ID
    :param string p_actionId: (required in path) Action ID
    """

    try:

        hub.log.debug("GET /deployment/api/resources/{resourceId}/actions/{actionId}")

        api = ResourceActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_resource_action(
            resource_id=p_resourceId, action_id=p_actionId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourceActionsApi.get_resource_action: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_resource_actions(hub, ctx, p_resourceId, **kwargs):
    """Fetch available resource actions. Returns the complete list of available actions that can be performed on a given
      resource. Performs GET /deployment/api/resources/{resourceId}/actions


    :param string p_resourceId: (required in path) Resource ID
    """

    try:

        hub.log.debug("GET /deployment/api/resources/{resourceId}/actions")

        api = ResourceActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_resource_actions(resource_id=p_resourceId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourceActionsApi.get_resource_actions: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def submit_resource_action_request(hub, ctx, p_resourceId, **kwargs):
    """Resource action request. Submit a resource action request. Performs POST /deployment/api/resources/{resourceId}/requests


    :param string p_resourceId: (required in path) Resource ID
    :param string actionId: (optional in body) The id of the action to perform.
    :param string reason: (optional in body) Reason for requesting a day2 operation
    :param object inputs: (optional in body)
    """

    try:

        hub.log.debug("POST /deployment/api/resources/{resourceId}/requests")

        api = ResourceActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}

        if "actionId" in kwargs:
            hub.log.debug(f"Got kwarg 'actionId' = {kwargs['actionId']}")
            body["actionId"] = kwargs.get("actionId")
            del kwargs["actionId"]
        if "reason" in kwargs:
            hub.log.debug(f"Got kwarg 'reason' = {kwargs['reason']}")
            body["reason"] = kwargs.get("reason")
            del kwargs["reason"]
        if "inputs" in kwargs:
            hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
            body["inputs"] = kwargs.get("inputs")
            del kwargs["inputs"]

        ret = api.submit_resource_action_request(
            body, resource_id=p_resourceId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourceActionsApi.submit_resource_action_request: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
