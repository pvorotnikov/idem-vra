from idem_vra.client.vra_catalog_lib.api import NotificationScenarioConfigurationApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_or_update(hub, ctx, scenarioId, enabled, **kwargs):
    """Creates or updates a notification scenario configuration of an organization  Performs POST /notification/api/scenario-configs


    :param string scenarioId: (required in body) Notification scenario id
    :param boolean enabled: (required in body) Notification scenario enabled
    :param boolean expandBody: (optional in query) The body field of each scenario configuration will be retrieved.
    :param string scenarioName: (optional in body) Notification scenario name
    :param string scenarioDescription: (optional in body) Notification scenario description
    :param string scenarioCategory: (optional in body) Notification scenario category
    :param string subject: (optional in body) Notification scenario subject
    :param string body: (optional in body) Notification scenario body
    """

    try:

        hub.log.debug("POST /notification/api/scenario-configs")

        api = NotificationScenarioConfigurationApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        body = {}
        body["scenarioId"] = scenarioId
        body["enabled"] = enabled

        if "scenarioName" in kwargs:
            hub.log.debug(f"Got kwarg 'scenarioName' = {kwargs['scenarioName']}")
            body["scenarioName"] = kwargs.get("scenarioName")
            del kwargs["scenarioName"]
        if "scenarioDescription" in kwargs:
            hub.log.debug(
                f"Got kwarg 'scenarioDescription' = {kwargs['scenarioDescription']}"
            )
            body["scenarioDescription"] = kwargs.get("scenarioDescription")
            del kwargs["scenarioDescription"]
        if "scenarioCategory" in kwargs:
            hub.log.debug(
                f"Got kwarg 'scenarioCategory' = {kwargs['scenarioCategory']}"
            )
            body["scenarioCategory"] = kwargs.get("scenarioCategory")
            del kwargs["scenarioCategory"]
        if "subject" in kwargs:
            hub.log.debug(f"Got kwarg 'subject' = {kwargs['subject']}")
            body["subject"] = kwargs.get("subject")
            del kwargs["subject"]
        if "body" in kwargs:
            hub.log.debug(f"Got kwarg 'body' = {kwargs['body']}")
            body["body"] = kwargs.get("body")
            del kwargs["body"]

        ret = api.create_or_update(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NotificationScenarioConfigurationApi.create_or_update: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete(hub, ctx, p_scenarioId, **kwargs):
    """Deletes a notification scenario configuration by scenario id of an organization  Performs DELETE /notification/api/scenario-configs/{scenarioId}


    :param string p_scenarioId: (required in path) Notification scenario Id
    """

    try:

        hub.log.debug("DELETE /notification/api/scenario-configs/{scenarioId}")

        api = NotificationScenarioConfigurationApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        ret = api.delete(scenario_id=p_scenarioId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NotificationScenarioConfigurationApi.delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_scenario_configs(hub, ctx, **kwargs):
    """Retrieves all notification scenario configurations of an organization  Performs GET /notification/api/scenario-configs


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /notification/api/scenario-configs")

        api = NotificationScenarioConfigurationApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        ret = api.get_all_scenario_configs(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NotificationScenarioConfigurationApi.get_all_scenario_configs: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_scenario_config(hub, ctx, p_scenarioId, **kwargs):
    """Retrieves a notification scenario configuration by scenario id of an
      organization  Performs GET /notification/api/scenario-configs/{scenarioId}


    :param string p_scenarioId: (required in path) Notification Scenario Id
    :param boolean expandBody: (optional in query) The body field of each scenario configuration will be retrieved.
    :param boolean defaultConfig: (optional in query) The defaultConfig of each scenario will be retrieved.
    """

    try:

        hub.log.debug("GET /notification/api/scenario-configs/{scenarioId}")

        api = NotificationScenarioConfigurationApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        ret = api.get_scenario_config(scenario_id=p_scenarioId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NotificationScenarioConfigurationApi.get_scenario_config: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_config(hub, ctx, scenarioId, enabled, **kwargs):
    """Enable or disabled a notification scenario configuration of an organization  Performs PATCH /notification/api/scenario-configs


    :param string scenarioId: (required in body) Notification scenario id
    :param boolean enabled: (required in body) Notification scenario enabled
    """

    try:

        hub.log.debug("PATCH /notification/api/scenario-configs")

        api = NotificationScenarioConfigurationApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        body = {}
        body["scenarioId"] = scenarioId
        body["enabled"] = enabled

        ret = api.patch_config(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NotificationScenarioConfigurationApi.patch_config: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def preview_notification_config(hub, ctx, p_scenarioId, subject, body, **kwargs):
    """Preview Notification Scenario Config  Performs POST /notification/api/scenario-configs/{scenarioId}/preview


    :param string p_scenarioId: (required in path) Notification Scenario Id
    :param string subject: (required in body) Notification scenario subject
    :param string body: (required in body) Notification scenario body
    :param object data: (optional in body)
    """

    try:

        hub.log.debug("POST /notification/api/scenario-configs/{scenarioId}/preview")

        api = NotificationScenarioConfigurationApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        body = {}
        body["subject"] = subject
        body["body"] = body

        if "data" in kwargs:
            hub.log.debug(f"Got kwarg 'data' = {kwargs['data']}")
            body["data"] = kwargs.get("data")
            del kwargs["data"]

        ret = api.preview_notification_config(body, scenario_id=p_scenarioId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NotificationScenarioConfigurationApi.preview_notification_config: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def send_test_notification(hub, ctx, p_scenarioId, recipient, **kwargs):
    """Send Test Notification Scenario Config  Performs POST /notification/api/scenario-configs/{scenarioId}/test


    :param string p_scenarioId: (required in path) Notification Scenario Id
    :param string recipient: (required in body) Recipient email
    :param object data: (optional in body)
    """

    try:

        hub.log.debug("POST /notification/api/scenario-configs/{scenarioId}/test")

        api = NotificationScenarioConfigurationApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        body = {}
        body["recipient"] = recipient

        if "data" in kwargs:
            hub.log.debug(f"Got kwarg 'data' = {kwargs['data']}")
            body["data"] = kwargs.get("data")
            del kwargs["data"]

        ret = api.send_test_notification(body, scenario_id=p_scenarioId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NotificationScenarioConfigurationApi.send_test_notification: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
