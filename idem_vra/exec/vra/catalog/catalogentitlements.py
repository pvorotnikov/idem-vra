from idem_vra.client.vra_catalog_lib.api import CatalogEntitlementsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_entitlement(hub, ctx, id, projectId, definition, **kwargs):
    """Create an entitlement. Creates an entitlement for a given project. Performs POST /catalog/api/admin/entitlements


    :param string id: (required in body) Entitlement id
    :param string projectId: (required in body) Project id
    :param Any definition: (required in body)
    :param boolean migrated: (optional in body) Migrated flag for entitlements
    """

    try:

        hub.log.debug("POST /catalog/api/admin/entitlements")

        api = CatalogEntitlementsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}
        body["id"] = id
        body["projectId"] = projectId
        body["definition"] = definition

        if "migrated" in kwargs:
            hub.log.debug(f"Got kwarg 'migrated' = {kwargs['migrated']}")
            body["migrated"] = kwargs.get("migrated")
            del kwargs["migrated"]

        ret = api.create_entitlement(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogEntitlementsApi.create_entitlement: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_entitlement(hub, ctx, p_id, **kwargs):
    """Delete an entitlement. Deletes the entitlement with the specified id. Performs DELETE /catalog/api/admin/entitlements/{id}


    :param string p_id: (required in path) Entitlement id
    """

    try:

        hub.log.debug("DELETE /catalog/api/admin/entitlements/{id}")

        api = CatalogEntitlementsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.delete_entitlement(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogEntitlementsApi.delete_entitlement: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_entitlements(hub, ctx, q_projectId, **kwargs):
    """Returns all entitlements filtered by projectId. Returns all entitlements (filtered by projectId). Performs GET /catalog/api/admin/entitlements


    :param string q_projectId: (required in query) The project id for which to return .
    """

    try:

        hub.log.debug("GET /catalog/api/admin/entitlements")

        api = CatalogEntitlementsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_entitlements(project_id=q_projectId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogEntitlementsApi.get_entitlements: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
