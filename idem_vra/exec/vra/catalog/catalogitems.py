from idem_vra.client.vra_catalog_lib.api import CatalogItemsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_catalog_item(hub, ctx, p_id, **kwargs):
    """Find a catalog item with specified ID. Returns the catalog item with the specified ID. Performs GET /catalog/api/items/{id}


    :param string p_id: (required in path) Catalog item ID
    :param boolean expandProjects: (optional in query) Retrieves the projects field of the catalog item
    """

    try:

        hub.log.debug("GET /catalog/api/items/{id}")

        api = CatalogItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_catalog_item(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogItemsApi.get_catalog_item: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_catalog_items(hub, ctx, **kwargs):
    """Fetch a list of catalog items. Returns a paginated list of catalog items. Performs GET /catalog/api/items


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param string search: (optional in query) Matches will have this string somewhere in their name or description.
    :param array projects: (optional in query) A list of project IDs. Results will belong to one of these projects.
    :param array types: (optional in query) A list of Catalog Item Type IDs. Results will be one of these types.
    :param boolean expandProjects: (optional in query) Whether or not to return detailed project data for each result.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /catalog/api/items")

        api = CatalogItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_catalog_items(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogItemsApi.get_catalog_items: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_upfront_price_response_for_catalog_item(
    hub, ctx, p_id, p_upfrontPriceId, **kwargs
):
    """Get a response with upfront prices for a given catalog item. Returns upfront prices of a given catalog item. Performs GET /catalog/api/items/{id}/upfront-prices/{upfrontPriceId}


    :param string p_id: (required in path) Catalog Item ID
    :param string p_upfrontPriceId: (required in path) Upfront Price Request ID
    """

    try:

        hub.log.debug("GET /catalog/api/items/{id}/upfront-prices/{upfrontPriceId}")

        api = CatalogItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_upfront_price_response_for_catalog_item(
            id=p_id, upfront_price_id=p_upfrontPriceId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogItemsApi.get_upfront_price_response_for_catalog_item: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_version_by_id(hub, ctx, p_id, p_versionId, **kwargs):
    """Fetch detailed catalog item version. Returns a detailed catalog item version. Performs GET /catalog/api/items/{id}/versions/{versionId}


    :param string p_id: (required in path) Catalog Item ID
    :param string p_versionId: (required in path) Catalog Item Version ID
    """

    try:

        hub.log.debug("GET /catalog/api/items/{id}/versions/{versionId}")

        api = CatalogItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_version_by_id(id=p_id, version_id=p_versionId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogItemsApi.get_version_by_id: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_versions(hub, ctx, p_id, **kwargs):
    """Fetch a list of catalog items with versions. Returns a paginated list of catalog item versions. Performs GET /catalog/api/items/{id}/versions


    :param string p_id: (required in path) Catalog Item ID
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /catalog/api/items/{id}/versions")

        api = CatalogItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_versions(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogItemsApi.get_versions: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def request_catalog_item_instances1(hub, ctx, p_id, **kwargs):
    """Create a deployment. Creates a deployment from a catalog item. Performs POST /catalog/api/items/{id}/request


    :param string p_id: (required in path) Catalog item ID
    :param string reason: (optional in body) Reason for request
    :param string deploymentName: (optional in body) Name of the requested deployment
    :param string projectId: (optional in body) Project to be used for the request
    :param object inputs: (optional in body)
    :param string version: (optional in body) Version of the catalog item. e.g. v2.0
    :param integer bulkRequestCount: (optional in body) Deployment request count defaults to 1 if not specified.
    """

    try:

        hub.log.debug("POST /catalog/api/items/{id}/request")

        api = CatalogItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}

        if "reason" in kwargs:
            hub.log.debug(f"Got kwarg 'reason' = {kwargs['reason']}")
            body["reason"] = kwargs.get("reason")
            del kwargs["reason"]
        if "deploymentName" in kwargs:
            hub.log.debug(f"Got kwarg 'deploymentName' = {kwargs['deploymentName']}")
            body["deploymentName"] = kwargs.get("deploymentName")
            del kwargs["deploymentName"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "inputs" in kwargs:
            hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
            body["inputs"] = kwargs.get("inputs")
            del kwargs["inputs"]
        if "version" in kwargs:
            hub.log.debug(f"Got kwarg 'version' = {kwargs['version']}")
            body["version"] = kwargs.get("version")
            del kwargs["version"]
        if "bulkRequestCount" in kwargs:
            hub.log.debug(
                f"Got kwarg 'bulkRequestCount' = {kwargs['bulkRequestCount']}"
            )
            body["bulkRequestCount"] = kwargs.get("bulkRequestCount")
            del kwargs["bulkRequestCount"]

        ret = api.request_catalog_item_instances1(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogItemsApi.request_catalog_item_instances1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def submit_upfront_price_request_for_catalog_item(hub, ctx, p_id, **kwargs):
    """Create a request to calculate upfront price for a given catalog item. Returns upfront price response for a given catalog item. Performs POST /catalog/api/items/{id}/upfront-prices


    :param string p_id: (required in path) Catalog Item ID
    :param string reason: (optional in body) Reason for request
    :param string deploymentName: (optional in body) Name of the requested deployment
    :param string projectId: (optional in body) Project to be used for the request
    :param object inputs: (optional in body)
    :param string version: (optional in body) Version of the catalog item. e.g. v2.0
    :param integer bulkRequestCount: (optional in body) Deployment request count defaults to 1 if not specified.
    """

    try:

        hub.log.debug("POST /catalog/api/items/{id}/upfront-prices")

        api = CatalogItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}

        if "reason" in kwargs:
            hub.log.debug(f"Got kwarg 'reason' = {kwargs['reason']}")
            body["reason"] = kwargs.get("reason")
            del kwargs["reason"]
        if "deploymentName" in kwargs:
            hub.log.debug(f"Got kwarg 'deploymentName' = {kwargs['deploymentName']}")
            body["deploymentName"] = kwargs.get("deploymentName")
            del kwargs["deploymentName"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "inputs" in kwargs:
            hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
            body["inputs"] = kwargs.get("inputs")
            del kwargs["inputs"]
        if "version" in kwargs:
            hub.log.debug(f"Got kwarg 'version' = {kwargs['version']}")
            body["version"] = kwargs.get("version")
            del kwargs["version"]
        if "bulkRequestCount" in kwargs:
            hub.log.debug(
                f"Got kwarg 'bulkRequestCount' = {kwargs['bulkRequestCount']}"
            )
            body["bulkRequestCount"] = kwargs.get("bulkRequestCount")
            del kwargs["bulkRequestCount"]

        ret = api.submit_upfront_price_request_for_catalog_item(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogItemsApi.submit_upfront_price_request_for_catalog_item: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
