from idem_vra.client.vra_catalog_lib.api import DeploymentActionsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_deployment_action(hub, ctx, p_deploymentId, p_actionId, **kwargs):
    """Fetch deployment action. Returns an action for the deployment specified by its Deployment ID and Action
      ID. Performs GET /deployment/api/deployments/{deploymentId}/actions/{actionId}


    :param string p_deploymentId: (required in path) Deployment ID
    :param string p_actionId: (required in path) Action ID
    """

    try:

        hub.log.debug(
            "GET /deployment/api/deployments/{deploymentId}/actions/{actionId}"
        )

        api = DeploymentActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_deployment_action(
            deployment_id=p_deploymentId, action_id=p_actionId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentActionsApi.get_deployment_action: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_deployment_actions(hub, ctx, p_deploymentId, **kwargs):
    """Fetch deployment available actions. Returns the complete list of available actions that can be performed on a given
      deployment. Performs GET /deployment/api/deployments/{deploymentId}/actions


    :param string p_deploymentId: (required in path) Deployment ID
    """

    try:

        hub.log.debug("GET /deployment/api/deployments/{deploymentId}/actions")

        api = DeploymentActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_deployment_actions(deployment_id=p_deploymentId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentActionsApi.get_deployment_actions: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_resource_action1(
    hub, ctx, p_deploymentId, p_resourceId, p_actionId, **kwargs
):
    """Fetch resource action. Returns an action for the resource specified by its Resource ID and Action ID. Performs GET /deployment/api/deployments/{deploymentId}/resources/{resourceId}/actions/{actionId}


    :param string p_deploymentId: (required in path) Deployment ID
    :param string p_resourceId: (required in path) Resource ID
    :param string p_actionId: (required in path) Action ID
    """

    try:

        hub.log.debug(
            "GET /deployment/api/deployments/{deploymentId}/resources/{resourceId}/actions/{actionId}"
        )

        api = DeploymentActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_resource_action1(
            deployment_id=p_deploymentId,
            resource_id=p_resourceId,
            action_id=p_actionId,
            **kwargs,
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentActionsApi.get_resource_action1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_resource_actions1(hub, ctx, p_deploymentId, p_resourceId, **kwargs):
    """Fetch available resource actions. Returns the complete list of available actions that can be performed on a given
      resource. Performs GET /deployment/api/deployments/{deploymentId}/resources/{resourceId}/actions


    :param string p_deploymentId: (required in path) Deployment ID
    :param string p_resourceId: (required in path) Resource ID
    """

    try:

        hub.log.debug(
            "GET /deployment/api/deployments/{deploymentId}/resources/{resourceId}/actions"
        )

        api = DeploymentActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_resource_actions1(
            deployment_id=p_deploymentId, resource_id=p_resourceId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentActionsApi.get_resource_actions1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def submit_deployment_action_request(hub, ctx, p_deploymentId, **kwargs):
    """Deployment action request. Submit a deployment action request Performs POST /deployment/api/deployments/{deploymentId}/requests


    :param string p_deploymentId: (required in path) Deployment ID
    :param string actionId: (optional in body) The id of the action to perform.
    :param string reason: (optional in body) Reason for requesting a day2 operation
    :param object inputs: (optional in body)
    """

    try:

        hub.log.debug("POST /deployment/api/deployments/{deploymentId}/requests")

        api = DeploymentActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}

        if "actionId" in kwargs:
            hub.log.debug(f"Got kwarg 'actionId' = {kwargs['actionId']}")
            body["actionId"] = kwargs.get("actionId")
            del kwargs["actionId"]
        if "reason" in kwargs:
            hub.log.debug(f"Got kwarg 'reason' = {kwargs['reason']}")
            body["reason"] = kwargs.get("reason")
            del kwargs["reason"]
        if "inputs" in kwargs:
            hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
            body["inputs"] = kwargs.get("inputs")
            del kwargs["inputs"]

        ret = api.submit_deployment_action_request(
            body, deployment_id=p_deploymentId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentActionsApi.submit_deployment_action_request: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def submit_resource_action_request1(
    hub, ctx, p_deploymentId, p_resourceId, **kwargs
):
    """Resource action request. Submit a resource action request. Performs POST /deployment/api/deployments/{deploymentId}/resources/{resourceId}/requests


    :param string p_deploymentId: (required in path) Deployment ID
    :param string p_resourceId: (required in path) Resource ID
    :param string actionId: (optional in body) The id of the action to perform.
    :param string reason: (optional in body) Reason for requesting a day2 operation
    :param object inputs: (optional in body)
    """

    try:

        hub.log.debug(
            "POST /deployment/api/deployments/{deploymentId}/resources/{resourceId}/requests"
        )

        api = DeploymentActionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}

        if "actionId" in kwargs:
            hub.log.debug(f"Got kwarg 'actionId' = {kwargs['actionId']}")
            body["actionId"] = kwargs.get("actionId")
            del kwargs["actionId"]
        if "reason" in kwargs:
            hub.log.debug(f"Got kwarg 'reason' = {kwargs['reason']}")
            body["reason"] = kwargs.get("reason")
            del kwargs["reason"]
        if "inputs" in kwargs:
            hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
            body["inputs"] = kwargs.get("inputs")
            del kwargs["inputs"]

        ret = api.submit_resource_action_request1(
            body, deployment_id=p_deploymentId, resource_id=p_resourceId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentActionsApi.submit_resource_action_request1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
