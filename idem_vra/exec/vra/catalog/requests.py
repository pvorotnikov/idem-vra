from idem_vra.client.vra_catalog_lib.api import RequestsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def action_deployment_request(hub, ctx, p_requestId, q_action, **kwargs):
    """Submit action on requests. Allowable values: cancel, dismiss. Cancel can be submitted on In-progress requests and Dismiss can be submitted on
      Failed requests. Performs POST /deployment/api/requests/{requestId}


    :param string p_requestId: (required in path)
    :param string q_action: (required in query)
    """

    try:

        hub.log.debug("POST /deployment/api/requests/{requestId}")

        api = RequestsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.action_deployment_request(
            request_id=p_requestId, action=q_action, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking RequestsApi.action_deployment_request: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_deployment_requests(hub, ctx, p_deploymentId, **kwargs):
    """Fetch deployment requests. Returns the requests for the deployment. Performs GET /deployment/api/deployments/{deploymentId}/requests


    :param string p_deploymentId: (required in path) Deployment ID
    :param boolean deleted: (optional in query) Retrieves the soft-deleted requests that have not yet been completely
      deleted.
    :param boolean inprogressRequests: (optional in query) Retrieves the requests that are currently in-progress for a
      deployment. In case of a false value the param is ignored.
    :param string search: (optional in query) Given string should be part of a searchable field of a deployment
      level request
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /deployment/api/deployments/{deploymentId}/requests")

        api = RequestsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_deployment_requests(deployment_id=p_deploymentId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking RequestsApi.get_deployment_requests: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_event_logs_content(hub, ctx, p_requestId, p_eventId, **kwargs):
    """Fetch Event logs content as a file. Returns the log file for an event. Performs GET /deployment/api/requests/{requestId}/events/{eventId}/logs/download


    :param string p_requestId: (required in path) Request ID
    :param string p_eventId: (required in path) Event ID
    """

    try:

        hub.log.debug(
            "GET /deployment/api/requests/{requestId}/events/{eventId}/logs/download"
        )

        api = RequestsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_event_logs_content(
            request_id=p_requestId, event_id=p_eventId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking RequestsApi.get_event_logs_content: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_event_logs(hub, ctx, p_requestId, p_eventId, **kwargs):
    """Fetch Event logs. Returns the logs for an event. Performs GET /deployment/api/requests/{requestId}/events/{eventId}/logs


    :param string p_requestId: (required in path) Request ID
    :param string p_eventId: (required in path) Event ID
    :param integer sinceRow: (optional in query) A positive row number from which to show logs.
    """

    try:

        hub.log.debug("GET /deployment/api/requests/{requestId}/events/{eventId}/logs")

        api = RequestsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_event_logs(request_id=p_requestId, event_id=p_eventId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking RequestsApi.get_event_logs: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_request_events(hub, ctx, p_requestId, **kwargs):
    """Fetch Request events. Returns all the events for a request. Performs GET /deployment/api/requests/{requestId}/events


    :param string p_requestId: (required in path) Request ID
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /deployment/api/requests/{requestId}/events")

        api = RequestsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_request_events(request_id=p_requestId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking RequestsApi.get_request_events: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_request(hub, ctx, p_requestId, **kwargs):
    """Get the Request. Returns the request with the given ID. Performs GET /deployment/api/requests/{requestId}


    :param string p_requestId: (required in path) Request ID
    """

    try:

        hub.log.debug("GET /deployment/api/requests/{requestId}")

        api = RequestsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_request(request_id=p_requestId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking RequestsApi.get_request: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list_resource_requests(hub, ctx, p_resourceId, **kwargs):
    """Get all requests for a resource  Performs GET /deployment/api/resources/{resourceId}/requests


    :param string p_resourceId: (required in path) Resource ID
    :param boolean inprogressRequests: (optional in query) Retrieves the requests that are currently in-progress for a resource.
      In case of a false value the param is ignored.
    :param string search: (optional in query) Given string should be part of a searchable field of a resource level
      request
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /deployment/api/resources/{resourceId}/requests")

        api = RequestsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.list_resource_requests(resource_id=p_resourceId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking RequestsApi.list_resource_requests: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
