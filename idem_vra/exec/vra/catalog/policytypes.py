from idem_vra.client.vra_catalog_lib.api import PolicyTypesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_policy_type_by_id(hub, ctx, p_id, **kwargs):
    """Returns the policy type with the specified ID. Find a specific policy type based on the input policy type id. Performs GET /policy/api/policyTypes/{id}


    :param string p_id: (required in path) Policy type ID
    """

    try:

        hub.log.debug("GET /policy/api/policyTypes/{id}")

        api = PolicyTypesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_policy_type_by_id(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PolicyTypesApi.get_policy_type_by_id: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_policy_type_scope_schema(hub, ctx, p_id, **kwargs):
    """Returns the policy scope schema for the type with the specified ID. Return the policy scope schema for the given policy type. Performs GET /policy/api/policyTypes/{id}/scopeSchema


    :param string p_id: (required in path) Policy type ID
    """

    try:

        hub.log.debug("GET /policy/api/policyTypes/{id}/scopeSchema")

        api = PolicyTypesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_policy_type_scope_schema(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PolicyTypesApi.get_policy_type_scope_schema: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_types(hub, ctx, **kwargs):
    """Returns a paginated list of policy types. Find all the policy types available in the current org. Performs GET /policy/api/policyTypes


    :param boolean expandSchema: (optional in query) Retrieves the schema for this policy type
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /policy/api/policyTypes")

        api = PolicyTypesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_types(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PolicyTypesApi.get_types: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
