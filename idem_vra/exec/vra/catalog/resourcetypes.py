from idem_vra.client.vra_catalog_lib.api import ResourceTypesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_resource_type(hub, ctx, p_resourceTypeId, **kwargs):
    """Fetch a specific Resource type. Returns the Resource type with the supplied ID. Performs GET /deployment/api/resource-types/{resourceTypeId}


    :param string p_resourceTypeId: (required in path)
    """

    try:

        hub.log.debug("GET /deployment/api/resource-types/{resourceTypeId}")

        api = ResourceTypesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_resource_type(resource_type_id=p_resourceTypeId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourceTypesApi.get_resource_type: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list_resource_types(hub, ctx, **kwargs):
    """Fetch all Resource Types. Returns a paginated list of Resource Types. Performs GET /deployment/api/resource-types


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param array ids: (optional in query) Filter by list of resource type ids
    :param array projectIds: (optional in query) Filter by list of project ids
    :param string search: (optional in query) Search by name and description
    :param string providerId: (optional in query) Filter by provider ID
    :param boolean composableOnly: (optional in query) Include only composable resource types
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /deployment/api/resource-types")

        api = ResourceTypesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.list_resource_types(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourceTypesApi.list_resource_types: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
