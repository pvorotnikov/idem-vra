from idem_vra.client.vra_catalog_lib.api import NotificationScenarioBrandingApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_or_update1(hub, ctx, header, footer, **kwargs):
    """Creates or updates a notification scenario branding of an organization  Performs POST /notification/api/scenario-branding


    :param string header: (required in body) Notification scenario header
    :param string footer: (required in body) Notification scenario footer
    :param boolean customized: (optional in body)
    """

    try:

        hub.log.debug("POST /notification/api/scenario-branding")

        api = NotificationScenarioBrandingApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        body = {}
        body["header"] = header
        body["footer"] = footer

        if "customized" in kwargs:
            hub.log.debug(f"Got kwarg 'customized' = {kwargs['customized']}")
            body["customized"] = kwargs.get("customized")
            del kwargs["customized"]

        ret = api.create_or_update1(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NotificationScenarioBrandingApi.create_or_update1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_scenario_branding(hub, ctx, **kwargs):
    """Deletes a notification scenario branding  Performs DELETE /notification/api/scenario-branding"""

    try:

        hub.log.debug("DELETE /notification/api/scenario-branding")

        api = NotificationScenarioBrandingApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        ret = api.delete_scenario_branding(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NotificationScenarioBrandingApi.delete_scenario_branding: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_scenario_branding(hub, ctx, **kwargs):
    """Retrieves a notification scenario branding of an organization  Performs GET /notification/api/scenario-branding"""

    try:

        hub.log.debug("GET /notification/api/scenario-branding")

        api = NotificationScenarioBrandingApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        ret = api.get_scenario_branding(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NotificationScenarioBrandingApi.get_scenario_branding: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
