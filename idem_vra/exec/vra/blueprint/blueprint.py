from idem_vra.client.vra_blueprint_lib.api import BlueprintApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_blueprint_using_post(hub, ctx, **kwargs):
    """Creates a blueprint  Performs POST /blueprint/api/blueprints


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string id: (optional in body) Object ID
    :param string createdAt: (optional in body) Created time
    :param string createdBy: (optional in body) Created by
    :param string updatedAt: (optional in body) Updated time
    :param string updatedBy: (optional in body) Updated by
    :param string orgId: (optional in body) Org ID
    :param string projectId: (optional in body) Project ID
    :param string projectName: (optional in body) Project Name
    :param string selfLink: (optional in body) Blueprint self link
    :param string name: (optional in body) Blueprint name
    :param string description: (optional in body) Blueprint description
    :param string status: (optional in body) Blueprint status
    :param string content: (optional in body) Blueprint YAML content
    :param boolean valid: (optional in body) Validation result on update
    :param array validationMessages: (optional in body) Validation messages
    :param integer totalVersions: (optional in body) Total versions
    :param integer totalReleasedVersions: (optional in body) Total released versions
    :param boolean requestScopeOrg: (optional in body) Flag to indicate blueprint can be requested from any project in org
    :param string contentSourceId: (optional in body) Content source id
    :param string contentSourcePath: (optional in body) Content source path
    :param string contentSourceType: (optional in body) Content source type
    :param string contentSourceSyncStatus: (optional in body) Content source last sync status
    :param array contentSourceSyncMessages: (optional in body) Content source last sync messages
    :param string contentSourceSyncAt: (optional in body) Content source last sync time
    """

    try:

        hub.log.debug("POST /blueprint/api/blueprints")

        api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "createdBy" in kwargs:
            hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
            body["createdBy"] = kwargs.get("createdBy")
            del kwargs["createdBy"]
        if "updatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
            body["updatedAt"] = kwargs.get("updatedAt")
            del kwargs["updatedAt"]
        if "updatedBy" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedBy' = {kwargs['updatedBy']}")
            body["updatedBy"] = kwargs.get("updatedBy")
            del kwargs["updatedBy"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "projectName" in kwargs:
            hub.log.debug(f"Got kwarg 'projectName' = {kwargs['projectName']}")
            body["projectName"] = kwargs.get("projectName")
            del kwargs["projectName"]
        if "selfLink" in kwargs:
            hub.log.debug(f"Got kwarg 'selfLink' = {kwargs['selfLink']}")
            body["selfLink"] = kwargs.get("selfLink")
            del kwargs["selfLink"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "status" in kwargs:
            hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
            body["status"] = kwargs.get("status")
            del kwargs["status"]
        if "content" in kwargs:
            hub.log.debug(f"Got kwarg 'content' = {kwargs['content']}")
            body["content"] = kwargs.get("content")
            del kwargs["content"]
        if "valid" in kwargs:
            hub.log.debug(f"Got kwarg 'valid' = {kwargs['valid']}")
            body["valid"] = kwargs.get("valid")
            del kwargs["valid"]
        if "validationMessages" in kwargs:
            hub.log.debug(
                f"Got kwarg 'validationMessages' = {kwargs['validationMessages']}"
            )
            body["validationMessages"] = kwargs.get("validationMessages")
            del kwargs["validationMessages"]
        if "totalVersions" in kwargs:
            hub.log.debug(f"Got kwarg 'totalVersions' = {kwargs['totalVersions']}")
            body["totalVersions"] = kwargs.get("totalVersions")
            del kwargs["totalVersions"]
        if "totalReleasedVersions" in kwargs:
            hub.log.debug(
                f"Got kwarg 'totalReleasedVersions' = {kwargs['totalReleasedVersions']}"
            )
            body["totalReleasedVersions"] = kwargs.get("totalReleasedVersions")
            del kwargs["totalReleasedVersions"]
        if "requestScopeOrg" in kwargs:
            hub.log.debug(f"Got kwarg 'requestScopeOrg' = {kwargs['requestScopeOrg']}")
            body["requestScopeOrg"] = kwargs.get("requestScopeOrg")
            del kwargs["requestScopeOrg"]
        if "contentSourceId" in kwargs:
            hub.log.debug(f"Got kwarg 'contentSourceId' = {kwargs['contentSourceId']}")
            body["contentSourceId"] = kwargs.get("contentSourceId")
            del kwargs["contentSourceId"]
        if "contentSourcePath" in kwargs:
            hub.log.debug(
                f"Got kwarg 'contentSourcePath' = {kwargs['contentSourcePath']}"
            )
            body["contentSourcePath"] = kwargs.get("contentSourcePath")
            del kwargs["contentSourcePath"]
        if "contentSourceType" in kwargs:
            hub.log.debug(
                f"Got kwarg 'contentSourceType' = {kwargs['contentSourceType']}"
            )
            body["contentSourceType"] = kwargs.get("contentSourceType")
            del kwargs["contentSourceType"]
        if "contentSourceSyncStatus" in kwargs:
            hub.log.debug(
                f"Got kwarg 'contentSourceSyncStatus' = {kwargs['contentSourceSyncStatus']}"
            )
            body["contentSourceSyncStatus"] = kwargs.get("contentSourceSyncStatus")
            del kwargs["contentSourceSyncStatus"]
        if "contentSourceSyncMessages" in kwargs:
            hub.log.debug(
                f"Got kwarg 'contentSourceSyncMessages' = {kwargs['contentSourceSyncMessages']}"
            )
            body["contentSourceSyncMessages"] = kwargs.get("contentSourceSyncMessages")
            del kwargs["contentSourceSyncMessages"]
        if "contentSourceSyncAt" in kwargs:
            hub.log.debug(
                f"Got kwarg 'contentSourceSyncAt' = {kwargs['contentSourceSyncAt']}"
            )
            body["contentSourceSyncAt"] = kwargs.get("contentSourceSyncAt")
            del kwargs["contentSourceSyncAt"]

        ret = api.create_blueprint_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintApi.create_blueprint_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_blueprint_version_using_post(
    hub, ctx, p_blueprintId, version, **kwargs
):
    """Creates version for the given blueprint ID  Performs POST /blueprint/api/blueprints/{blueprintId}/versions


    :param string p_blueprintId: (required in path)
    :param string version: (required in body) Blueprint version
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string description: (optional in body) Blueprint version description
    :param string changeLog: (optional in body) Blueprint version change log
    :param boolean release: (optional in body) Flag indicating to release version
    """

    try:

        hub.log.debug("POST /blueprint/api/blueprints/{blueprintId}/versions")

        api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        body = {}
        body["version"] = version

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "changeLog" in kwargs:
            hub.log.debug(f"Got kwarg 'changeLog' = {kwargs['changeLog']}")
            body["changeLog"] = kwargs.get("changeLog")
            del kwargs["changeLog"]
        if "release" in kwargs:
            hub.log.debug(f"Got kwarg 'release' = {kwargs['release']}")
            body["release"] = kwargs.get("release")
            del kwargs["release"]

        ret = api.create_blueprint_version_using_post(
            body, blueprint_id=p_blueprintId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintApi.create_blueprint_version_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_blueprint_using_delete(hub, ctx, p_blueprintId, **kwargs):
    """Deletes a blueprint  Performs DELETE /blueprint/api/blueprints/{blueprintId}


    :param string p_blueprintId: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug("DELETE /blueprint/api/blueprints/{blueprintId}")

        api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.delete_blueprint_using_delete(blueprint_id=p_blueprintId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintApi.delete_blueprint_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_blueprint_inputs_schema_using_get(hub, ctx, p_blueprintId, **kwargs):
    """Returns blueprint inputs schema  Performs GET /blueprint/api/blueprints/{blueprintId}/inputs-schema


    :param string p_blueprintId: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug("GET /blueprint/api/blueprints/{blueprintId}/inputs-schema")

        api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.get_blueprint_inputs_schema_using_get(
            blueprint_id=p_blueprintId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintApi.get_blueprint_inputs_schema_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_blueprint_using_get(hub, ctx, p_blueprintId, **kwargs):
    """Returns blueprint details  Performs GET /blueprint/api/blueprints/{blueprintId}


    :param string p_blueprintId: (required in path)
    :param array select: (optional in query) Fields to include in content.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug("GET /blueprint/api/blueprints/{blueprintId}")

        api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.get_blueprint_using_get(blueprint_id=p_blueprintId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintApi.get_blueprint_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_blueprint_version_inputs_schema_using_get(
    hub, ctx, p_blueprintId, p_version, **kwargs
):
    """Returns blueprint version inputs schema  Performs GET /blueprint/api/blueprints/{blueprintId}/versions/{version}/inputs-schema


    :param string p_blueprintId: (required in path)
    :param string p_version: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug(
            "GET /blueprint/api/blueprints/{blueprintId}/versions/{version}/inputs-schema"
        )

        api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.get_blueprint_version_inputs_schema_using_get(
            blueprint_id=p_blueprintId, version=p_version, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintApi.get_blueprint_version_inputs_schema_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_blueprint_version_using_get(hub, ctx, p_blueprintId, p_version, **kwargs):
    """Returns versioned blueprint details  Performs GET /blueprint/api/blueprints/{blueprintId}/versions/{version}


    :param string p_blueprintId: (required in path)
    :param string p_version: (required in path)
    :param array select: (optional in query) Fields to include in content.
    """

    try:

        hub.log.debug("GET /blueprint/api/blueprints/{blueprintId}/versions/{version}")

        api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])

        ret = api.get_blueprint_version_using_get(
            blueprint_id=p_blueprintId, version=p_version, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintApi.get_blueprint_version_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list_blueprint_versions_using_get(hub, ctx, p_blueprintId, **kwargs):
    """Lists blueprint versions  Performs GET /blueprint/api/blueprints/{blueprintId}/versions


    :param string p_blueprintId: (required in path)
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param string version: (optional in query) Filter by version
    :param array select: (optional in query) Fields to include in content.
    :param string status: (optional in query) Filter by blueprint status: versioned / released
    :param array propertyGroups: (optional in query) Filter versions with any of the specified property groups
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is descending on updatedAt. Sorting is supported on fields
      createdAt, updatedAt, createdBy, updatedBy, name, version.
    """

    try:

        hub.log.debug("GET /blueprint/api/blueprints/{blueprintId}/versions")

        api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.list_blueprint_versions_using_get(
            blueprint_id=p_blueprintId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintApi.list_blueprint_versions_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list_blueprints_using_get(hub, ctx, **kwargs):
    """Lists draft blueprint  Performs GET /blueprint/api/blueprints


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param array select: (optional in query) Fields to include in content.
    :param string name: (optional in query) Filter by name
    :param string search: (optional in query) Search by name and description
    :param array projects: (optional in query) A comma-separated list. Results must be associated with one of these
      project IDs.
    :param array propertyGroups: (optional in query) Filter blueprints with any of the specified property groups
    :param boolean versioned: (optional in query) Filter blueprints with at least one version
    :param boolean released: (optional in query) Filter blueprints with at least one released version
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is descending on updatedAt. Sorting is supported on fields
      createdAt, updatedAt, createdBy, updatedBy, name.
    """

    try:

        hub.log.debug("GET /blueprint/api/blueprints")

        api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.list_blueprints_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintApi.list_blueprints_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def release_blueprint_version_using_post(
    hub, ctx, p_blueprintId, p_version, **kwargs
):
    """Release versioned blueprint to catalog  Performs POST /blueprint/api/blueprints/{blueprintId}/versions/{version}/actions/release


    :param string p_blueprintId: (required in path)
    :param string p_version: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug(
            "POST /blueprint/api/blueprints/{blueprintId}/versions/{version}/actions/release"
        )

        api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.release_blueprint_version_using_post(
            blueprint_id=p_blueprintId, version=p_version, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintApi.release_blueprint_version_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def restore_blueprint_version_using_post(
    hub, ctx, p_blueprintId, p_version, **kwargs
):
    """Restores content of draft from versioned blueprint  Performs POST /blueprint/api/blueprints/{blueprintId}/versions/{version}/actions/restore


    :param string p_blueprintId: (required in path)
    :param string p_version: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug(
            "POST /blueprint/api/blueprints/{blueprintId}/versions/{version}/actions/restore"
        )

        api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.restore_blueprint_version_using_post(
            blueprint_id=p_blueprintId, version=p_version, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintApi.restore_blueprint_version_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def un_release_blueprint_version_using_post(
    hub, ctx, p_blueprintId, p_version, **kwargs
):
    """UnRelease versioned blueprint from catalog  Performs POST /blueprint/api/blueprints/{blueprintId}/versions/{version}/actions/unrelease


    :param string p_blueprintId: (required in path)
    :param string p_version: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug(
            "POST /blueprint/api/blueprints/{blueprintId}/versions/{version}/actions/unrelease"
        )

        api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.un_release_blueprint_version_using_post(
            blueprint_id=p_blueprintId, version=p_version, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintApi.un_release_blueprint_version_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_blueprint_using_put(hub, ctx, p_blueprintId, **kwargs):
    """Updates a blueprint  Performs PUT /blueprint/api/blueprints/{blueprintId}


    :param string p_blueprintId: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string id: (optional in body) Object ID
    :param string createdAt: (optional in body) Created time
    :param string createdBy: (optional in body) Created by
    :param string updatedAt: (optional in body) Updated time
    :param string updatedBy: (optional in body) Updated by
    :param string orgId: (optional in body) Org ID
    :param string projectId: (optional in body) Project ID
    :param string projectName: (optional in body) Project Name
    :param string selfLink: (optional in body) Blueprint self link
    :param string name: (optional in body) Blueprint name
    :param string description: (optional in body) Blueprint description
    :param string status: (optional in body) Blueprint status
    :param string content: (optional in body) Blueprint YAML content
    :param boolean valid: (optional in body) Validation result on update
    :param array validationMessages: (optional in body) Validation messages
    :param integer totalVersions: (optional in body) Total versions
    :param integer totalReleasedVersions: (optional in body) Total released versions
    :param boolean requestScopeOrg: (optional in body) Flag to indicate blueprint can be requested from any project in org
    :param string contentSourceId: (optional in body) Content source id
    :param string contentSourcePath: (optional in body) Content source path
    :param string contentSourceType: (optional in body) Content source type
    :param string contentSourceSyncStatus: (optional in body) Content source last sync status
    :param array contentSourceSyncMessages: (optional in body) Content source last sync messages
    :param string contentSourceSyncAt: (optional in body) Content source last sync time
    """

    try:

        hub.log.debug("PUT /blueprint/api/blueprints/{blueprintId}")

        api = BlueprintApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "createdBy" in kwargs:
            hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
            body["createdBy"] = kwargs.get("createdBy")
            del kwargs["createdBy"]
        if "updatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
            body["updatedAt"] = kwargs.get("updatedAt")
            del kwargs["updatedAt"]
        if "updatedBy" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedBy' = {kwargs['updatedBy']}")
            body["updatedBy"] = kwargs.get("updatedBy")
            del kwargs["updatedBy"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "projectName" in kwargs:
            hub.log.debug(f"Got kwarg 'projectName' = {kwargs['projectName']}")
            body["projectName"] = kwargs.get("projectName")
            del kwargs["projectName"]
        if "selfLink" in kwargs:
            hub.log.debug(f"Got kwarg 'selfLink' = {kwargs['selfLink']}")
            body["selfLink"] = kwargs.get("selfLink")
            del kwargs["selfLink"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "status" in kwargs:
            hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
            body["status"] = kwargs.get("status")
            del kwargs["status"]
        if "content" in kwargs:
            hub.log.debug(f"Got kwarg 'content' = {kwargs['content']}")
            body["content"] = kwargs.get("content")
            del kwargs["content"]
        if "valid" in kwargs:
            hub.log.debug(f"Got kwarg 'valid' = {kwargs['valid']}")
            body["valid"] = kwargs.get("valid")
            del kwargs["valid"]
        if "validationMessages" in kwargs:
            hub.log.debug(
                f"Got kwarg 'validationMessages' = {kwargs['validationMessages']}"
            )
            body["validationMessages"] = kwargs.get("validationMessages")
            del kwargs["validationMessages"]
        if "totalVersions" in kwargs:
            hub.log.debug(f"Got kwarg 'totalVersions' = {kwargs['totalVersions']}")
            body["totalVersions"] = kwargs.get("totalVersions")
            del kwargs["totalVersions"]
        if "totalReleasedVersions" in kwargs:
            hub.log.debug(
                f"Got kwarg 'totalReleasedVersions' = {kwargs['totalReleasedVersions']}"
            )
            body["totalReleasedVersions"] = kwargs.get("totalReleasedVersions")
            del kwargs["totalReleasedVersions"]
        if "requestScopeOrg" in kwargs:
            hub.log.debug(f"Got kwarg 'requestScopeOrg' = {kwargs['requestScopeOrg']}")
            body["requestScopeOrg"] = kwargs.get("requestScopeOrg")
            del kwargs["requestScopeOrg"]
        if "contentSourceId" in kwargs:
            hub.log.debug(f"Got kwarg 'contentSourceId' = {kwargs['contentSourceId']}")
            body["contentSourceId"] = kwargs.get("contentSourceId")
            del kwargs["contentSourceId"]
        if "contentSourcePath" in kwargs:
            hub.log.debug(
                f"Got kwarg 'contentSourcePath' = {kwargs['contentSourcePath']}"
            )
            body["contentSourcePath"] = kwargs.get("contentSourcePath")
            del kwargs["contentSourcePath"]
        if "contentSourceType" in kwargs:
            hub.log.debug(
                f"Got kwarg 'contentSourceType' = {kwargs['contentSourceType']}"
            )
            body["contentSourceType"] = kwargs.get("contentSourceType")
            del kwargs["contentSourceType"]
        if "contentSourceSyncStatus" in kwargs:
            hub.log.debug(
                f"Got kwarg 'contentSourceSyncStatus' = {kwargs['contentSourceSyncStatus']}"
            )
            body["contentSourceSyncStatus"] = kwargs.get("contentSourceSyncStatus")
            del kwargs["contentSourceSyncStatus"]
        if "contentSourceSyncMessages" in kwargs:
            hub.log.debug(
                f"Got kwarg 'contentSourceSyncMessages' = {kwargs['contentSourceSyncMessages']}"
            )
            body["contentSourceSyncMessages"] = kwargs.get("contentSourceSyncMessages")
            del kwargs["contentSourceSyncMessages"]
        if "contentSourceSyncAt" in kwargs:
            hub.log.debug(
                f"Got kwarg 'contentSourceSyncAt' = {kwargs['contentSourceSyncAt']}"
            )
            body["contentSourceSyncAt"] = kwargs.get("contentSourceSyncAt")
            del kwargs["contentSourceSyncAt"]

        ret = api.update_blueprint_using_put(body, blueprint_id=p_blueprintId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintApi.update_blueprint_using_put: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
