from idem_vra.client.vra_blueprint_lib.api import BlueprintTerraformIntegrationsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_blueprint_from_mapping_using_post(hub, ctx, **kwargs):
    """Creates a blueprint from a Terraform blueprint configuration (obtained from
      /create-blueprint-mapping).  Performs POST /blueprint/api/blueprint-integrations/terraform/create-blueprint-from-mapping


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string id: (optional in body) Object ID
    :param string createdAt: (optional in body) Created time
    :param string createdBy: (optional in body) Created by
    :param string updatedAt: (optional in body) Updated time
    :param string updatedBy: (optional in body) Updated by
    :param string orgId: (optional in body) Org ID
    :param string projectId: (optional in body) Project ID
    :param string projectName: (optional in body) Project Name
    :param string name: (optional in body) Blueprint name.
    :param string description: (optional in body) Blueprint description.
    :param string version: (optional in body) Terraform runtime version.
    :param Any terraformToBlueprintMapping: (optional in body)
    """

    try:

        hub.log.debug(
            "POST /blueprint/api/blueprint-integrations/terraform/create-blueprint-from-mapping"
        )

        api = BlueprintTerraformIntegrationsApi(
            hub.clients["idem_vra.client.vra_blueprint_lib.api"]
        )
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "createdBy" in kwargs:
            hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
            body["createdBy"] = kwargs.get("createdBy")
            del kwargs["createdBy"]
        if "updatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
            body["updatedAt"] = kwargs.get("updatedAt")
            del kwargs["updatedAt"]
        if "updatedBy" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedBy' = {kwargs['updatedBy']}")
            body["updatedBy"] = kwargs.get("updatedBy")
            del kwargs["updatedBy"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "projectName" in kwargs:
            hub.log.debug(f"Got kwarg 'projectName' = {kwargs['projectName']}")
            body["projectName"] = kwargs.get("projectName")
            del kwargs["projectName"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "version" in kwargs:
            hub.log.debug(f"Got kwarg 'version' = {kwargs['version']}")
            body["version"] = kwargs.get("version")
            del kwargs["version"]
        if "terraformToBlueprintMapping" in kwargs:
            hub.log.debug(
                f"Got kwarg 'terraformToBlueprintMapping' = {kwargs['terraformToBlueprintMapping']}"
            )
            body["terraformToBlueprintMapping"] = kwargs.get(
                "terraformToBlueprintMapping"
            )
            del kwargs["terraformToBlueprintMapping"]

        ret = api.create_blueprint_from_mapping_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintTerraformIntegrationsApi.create_blueprint_from_mapping_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_blueprint_mapping_using_post(hub, ctx, **kwargs):
    """Retrieves and parses the specified Terraform configuration file(s) and returns
      relevant information for blueprint construction.  Performs POST /blueprint/api/blueprint-integrations/terraform/create-blueprint-mapping


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string sourceDirectory: (optional in body) A path to the desired Terraform directory.
    :param string repositoryId: (optional in body) The ID of the relevant configuration source repository.
    :param string commitId: (optional in body) ID that identifies the commit that corresponds to the desired version
      of the remote file(s).
    """

    try:

        hub.log.debug(
            "POST /blueprint/api/blueprint-integrations/terraform/create-blueprint-mapping"
        )

        api = BlueprintTerraformIntegrationsApi(
            hub.clients["idem_vra.client.vra_blueprint_lib.api"]
        )
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        body = {}

        if "sourceDirectory" in kwargs:
            hub.log.debug(f"Got kwarg 'sourceDirectory' = {kwargs['sourceDirectory']}")
            body["sourceDirectory"] = kwargs.get("sourceDirectory")
            del kwargs["sourceDirectory"]
        if "repositoryId" in kwargs:
            hub.log.debug(f"Got kwarg 'repositoryId' = {kwargs['repositoryId']}")
            body["repositoryId"] = kwargs.get("repositoryId")
            del kwargs["repositoryId"]
        if "commitId" in kwargs:
            hub.log.debug(f"Got kwarg 'commitId' = {kwargs['commitId']}")
            body["commitId"] = kwargs.get("commitId")
            del kwargs["commitId"]

        ret = api.create_blueprint_mapping_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintTerraformIntegrationsApi.create_blueprint_mapping_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_terraform_version_using_post(hub, ctx, **kwargs):
    """Creates a version  Performs POST /blueprint/api/blueprint-integrations/terraform/versions


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string id: (optional in body) Version ID
    :param string version: (optional in body) The numeric version of terraform release
    :param string description: (optional in body) Version description
    :param string url: (optional in body) Download url
    :param string authenticationType: (optional in body) The type of authentication for the download url
    :param string username: (optional in body) The user name for basic authentication
    :param string password: (optional in body) The password for basic authentication
    :param string sha256Checksum: (optional in body) The sha256 checksum of the terraform binary
    :param boolean enabled: (optional in body) Version status
    :param string createdAt: (optional in body) Created time
    :param string createdBy: (optional in body) Created by
    :param string updatedAt: (optional in body) Updated time
    :param string updatedBy: (optional in body) Updated by
    :param string orgId: (optional in body) Org ID
    """

    try:

        hub.log.debug("POST /blueprint/api/blueprint-integrations/terraform/versions")

        api = BlueprintTerraformIntegrationsApi(
            hub.clients["idem_vra.client.vra_blueprint_lib.api"]
        )
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "version" in kwargs:
            hub.log.debug(f"Got kwarg 'version' = {kwargs['version']}")
            body["version"] = kwargs.get("version")
            del kwargs["version"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "url" in kwargs:
            hub.log.debug(f"Got kwarg 'url' = {kwargs['url']}")
            body["url"] = kwargs.get("url")
            del kwargs["url"]
        if "authenticationType" in kwargs:
            hub.log.debug(
                f"Got kwarg 'authenticationType' = {kwargs['authenticationType']}"
            )
            body["authenticationType"] = kwargs.get("authenticationType")
            del kwargs["authenticationType"]
        if "username" in kwargs:
            hub.log.debug(f"Got kwarg 'username' = {kwargs['username']}")
            body["username"] = kwargs.get("username")
            del kwargs["username"]
        if "password" in kwargs:
            hub.log.debug(f"Got kwarg 'password' = {kwargs['password']}")
            body["password"] = kwargs.get("password")
            del kwargs["password"]
        if "sha256Checksum" in kwargs:
            hub.log.debug(f"Got kwarg 'sha256Checksum' = {kwargs['sha256Checksum']}")
            body["sha256Checksum"] = kwargs.get("sha256Checksum")
            del kwargs["sha256Checksum"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "createdBy" in kwargs:
            hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
            body["createdBy"] = kwargs.get("createdBy")
            del kwargs["createdBy"]
        if "updatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
            body["updatedAt"] = kwargs.get("updatedAt")
            del kwargs["updatedAt"]
        if "updatedBy" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedBy' = {kwargs['updatedBy']}")
            body["updatedBy"] = kwargs.get("updatedBy")
            del kwargs["updatedBy"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]

        ret = api.create_terraform_version_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintTerraformIntegrationsApi.create_terraform_version_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_terraform_version_using_delete(hub, ctx, p_versionId, **kwargs):
    """Deletes a terraform version  Performs DELETE /blueprint/api/blueprint-integrations/terraform/versions/{versionId}


    :param string p_versionId: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug(
            "DELETE /blueprint/api/blueprint-integrations/terraform/versions/{versionId}"
        )

        api = BlueprintTerraformIntegrationsApi(
            hub.clients["idem_vra.client.vra_blueprint_lib.api"]
        )
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.delete_terraform_version_using_delete(
            version_id=p_versionId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintTerraformIntegrationsApi.delete_terraform_version_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_configuration_source_tree_using_get(
    hub, ctx, q_configurationSourceId, **kwargs
):
    """Shows directories of the configuration source repository that correspond to
      Terraform configurations.  Performs GET /blueprint/api/blueprint-integrations/terraform/get-configuration-source-tree


    :param string q_configurationSourceId: (required in query) The ID of the configuration source to inspect.
    :param string commitId: (optional in query) The commit ID corresponding to the version of the configuration
      source.
    :param string path: (optional in query) A file path prefix. The prefix is interpreted in the context of the
      configuration sources path prefix. Results will only include
      directories under this path.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug(
            "GET /blueprint/api/blueprint-integrations/terraform/get-configuration-source-tree"
        )

        api = BlueprintTerraformIntegrationsApi(
            hub.clients["idem_vra.client.vra_blueprint_lib.api"]
        )
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.get_configuration_source_tree_using_get(
            configuration_source_id=q_configurationSourceId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintTerraformIntegrationsApi.get_configuration_source_tree_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_terraform_configuration_source_commit_list_using_get(
    hub, ctx, q_configurationSourceId, **kwargs
):
    """Returns a paginated list of commits for a specified configuration source.  Performs GET /blueprint/api/blueprint-integrations/terraform/get-configuration-source-commits


    :param string q_configurationSourceId: (required in query) A Configuration Source ID.
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug(
            "GET /blueprint/api/blueprint-integrations/terraform/get-configuration-source-commits"
        )

        api = BlueprintTerraformIntegrationsApi(
            hub.clients["idem_vra.client.vra_blueprint_lib.api"]
        )
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.get_terraform_configuration_source_commit_list_using_get(
            configuration_source_id=q_configurationSourceId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintTerraformIntegrationsApi.get_terraform_configuration_source_commit_list_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_terraform_configuration_sources_using_get(hub, ctx, q_projects, **kwargs):
    """Returns a paginated list of configuration sources configured as storage for
      Terraform configurations.  Performs GET /blueprint/api/blueprint-integrations/terraform/get-configuration-sources


    :param array q_projects: (required in query) A comma-separated list of project IDs. Results will be filtered so
      only configuration sources accessible from one or more of these
      projects will be returned.
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param string search: (optional in query) A search parameter to search based on configuration source name or
      repository.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug(
            "GET /blueprint/api/blueprint-integrations/terraform/get-configuration-sources"
        )

        api = BlueprintTerraformIntegrationsApi(
            hub.clients["idem_vra.client.vra_blueprint_lib.api"]
        )
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.get_terraform_configuration_sources_using_get(
            projects=q_projects, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintTerraformIntegrationsApi.get_terraform_configuration_sources_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_terraform_version_using_get(hub, ctx, p_versionId, **kwargs):
    """Returns Terraform Version details  Performs GET /blueprint/api/blueprint-integrations/terraform/versions/{versionId}


    :param string p_versionId: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug(
            "GET /blueprint/api/blueprint-integrations/terraform/versions/{versionId}"
        )

        api = BlueprintTerraformIntegrationsApi(
            hub.clients["idem_vra.client.vra_blueprint_lib.api"]
        )
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.get_terraform_version_using_get(version_id=p_versionId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintTerraformIntegrationsApi.get_terraform_version_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list_terraform_versions_using_get(hub, ctx, q_pageable, **kwargs):
    """Lists terraform versions  Performs GET /blueprint/api/blueprint-integrations/terraform/versions


    :param None q_pageable: (required in query)
    :param boolean onlyEnabled: (optional in query) Include only enabled versions
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is descending on updatedAt. Sorting is supported on fields
      createdAt, updatedAt, createdBy, updatedBy, version.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug("GET /blueprint/api/blueprint-integrations/terraform/versions")

        api = BlueprintTerraformIntegrationsApi(
            hub.clients["idem_vra.client.vra_blueprint_lib.api"]
        )
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.list_terraform_versions_using_get(pageable=q_pageable, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintTerraformIntegrationsApi.list_terraform_versions_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_terraform_version_using_patch(hub, ctx, p_versionId, **kwargs):
    """Updates a terraform version  Performs PATCH /blueprint/api/blueprint-integrations/terraform/versions/{versionId}


    :param string p_versionId: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string id: (optional in body) Version ID
    :param string version: (optional in body) The numeric version of terraform release
    :param string description: (optional in body) Version description
    :param string url: (optional in body) Download url
    :param string authenticationType: (optional in body) The type of authentication for the download url
    :param string username: (optional in body) The user name for basic authentication
    :param string password: (optional in body) The password for basic authentication
    :param string sha256Checksum: (optional in body) The sha256 checksum of the terraform binary
    :param boolean enabled: (optional in body) Version status
    :param string createdAt: (optional in body) Created time
    :param string createdBy: (optional in body) Created by
    :param string updatedAt: (optional in body) Updated time
    :param string updatedBy: (optional in body) Updated by
    :param string orgId: (optional in body) Org ID
    """

    try:

        hub.log.debug(
            "PATCH /blueprint/api/blueprint-integrations/terraform/versions/{versionId}"
        )

        api = BlueprintTerraformIntegrationsApi(
            hub.clients["idem_vra.client.vra_blueprint_lib.api"]
        )
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "version" in kwargs:
            hub.log.debug(f"Got kwarg 'version' = {kwargs['version']}")
            body["version"] = kwargs.get("version")
            del kwargs["version"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "url" in kwargs:
            hub.log.debug(f"Got kwarg 'url' = {kwargs['url']}")
            body["url"] = kwargs.get("url")
            del kwargs["url"]
        if "authenticationType" in kwargs:
            hub.log.debug(
                f"Got kwarg 'authenticationType' = {kwargs['authenticationType']}"
            )
            body["authenticationType"] = kwargs.get("authenticationType")
            del kwargs["authenticationType"]
        if "username" in kwargs:
            hub.log.debug(f"Got kwarg 'username' = {kwargs['username']}")
            body["username"] = kwargs.get("username")
            del kwargs["username"]
        if "password" in kwargs:
            hub.log.debug(f"Got kwarg 'password' = {kwargs['password']}")
            body["password"] = kwargs.get("password")
            del kwargs["password"]
        if "sha256Checksum" in kwargs:
            hub.log.debug(f"Got kwarg 'sha256Checksum' = {kwargs['sha256Checksum']}")
            body["sha256Checksum"] = kwargs.get("sha256Checksum")
            del kwargs["sha256Checksum"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "createdBy" in kwargs:
            hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
            body["createdBy"] = kwargs.get("createdBy")
            del kwargs["createdBy"]
        if "updatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
            body["updatedAt"] = kwargs.get("updatedAt")
            del kwargs["updatedAt"]
        if "updatedBy" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedBy' = {kwargs['updatedBy']}")
            body["updatedBy"] = kwargs.get("updatedBy")
            del kwargs["updatedBy"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]

        ret = api.update_terraform_version_using_patch(
            body, version_id=p_versionId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintTerraformIntegrationsApi.update_terraform_version_using_patch: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
