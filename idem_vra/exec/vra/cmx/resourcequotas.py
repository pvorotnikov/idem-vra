from idem_vra.client.vra_cmx_lib.api import ResourceQuotasApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get(hub, ctx, p_id, **kwargs):
    """Get a K8S ResourceQuota by id Get a K8S ResourceQuota by id Performs GET /cmx/api/resources/resource-quotas/{id}


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("GET /cmx/api/resources/resource-quotas/{id}")

        api = ResourceQuotasApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ResourceQuotasApi.get: {err}")
        return ExecReturn(result=False, comment=str(err))


async def list3(hub, ctx, **kwargs):
    """Get All K8S ResourceQuotas Get a list of all K8S ResourceQuotas Performs GET /cmx/api/resources/resource-quotas


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending.
    """

    try:

        hub.log.debug("GET /cmx/api/resources/resource-quotas")

        api = ResourceQuotasApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.list3(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourceQuotasApi.list3: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
