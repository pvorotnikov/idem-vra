from idem_vra.client.vra_cmx_lib.api import VSphereEndpointsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_full_virtual_machine_classes(hub, ctx, p_endpointSelfLinkId, **kwargs):
    """Get full information about all virtual machine classes for a vSphere endpoint Get all virtual machine classes defined in all managed supervisor clusters in a
      particular vSphere instance. vSphere instance is identified by endpoint
      SelfLink Id Performs GET /cmx/api/resources/vsphere/endpoints/{endpointSelfLinkId}/virtual-machine-classes-described


    :param string p_endpointSelfLinkId: (required in path)
    """

    try:

        hub.log.debug(
            "GET /cmx/api/resources/vsphere/endpoints/{endpointSelfLinkId}/virtual-machine-classes-described"
        )

        api = VSphereEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_full_virtual_machine_classes(
            endpoint_self_link_id=p_endpointSelfLinkId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking VSphereEndpointsApi.get_full_virtual_machine_classes: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_storage_classes(hub, ctx, p_endpointSelfLinkId, **kwargs):
    """Get all storage classes identifiers for a vSphere endpoint Get all storage classes defined in all managed supervisor clusters in a
      particular vSphere instance. vSphere instance is identified by endpoint
      SelfLink Id. This method is Deprecated. Instead, use /storage-classes from
      Vcenter Endpoints Performs GET /cmx/api/resources/vsphere/endpoints/{endpointSelfLinkId}/storage-classes


    :param string p_endpointSelfLinkId: (required in path)
    """

    try:

        hub.log.debug(
            "GET /cmx/api/resources/vsphere/endpoints/{endpointSelfLinkId}/storage-classes"
        )

        api = VSphereEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_storage_classes(
            endpoint_self_link_id=p_endpointSelfLinkId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking VSphereEndpointsApi.get_storage_classes: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_tanzu_kubernetes_releases(hub, ctx, p_endpointSelfLinkId, **kwargs):
    """Get all tanzu kubernetes releases identifiers for a vSphere endpoint Get all tanzu kubernetes releases supported in all managed supervisor clusters
      in a particular vSphere instance. vSphere instance is identified by endpoint
      SelfLink Id Performs GET /cmx/api/resources/vsphere/endpoints/{endpointSelfLinkId}/tanzu-kubernetes-releases


    :param string p_endpointSelfLinkId: (required in path)
    """

    try:

        hub.log.debug(
            "GET /cmx/api/resources/vsphere/endpoints/{endpointSelfLinkId}/tanzu-kubernetes-releases"
        )

        api = VSphereEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_tanzu_kubernetes_releases(
            endpoint_self_link_id=p_endpointSelfLinkId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking VSphereEndpointsApi.get_tanzu_kubernetes_releases: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_virtual_machine_classes(hub, ctx, p_endpointSelfLinkId, **kwargs):
    """Get all virtual machine classes names for a vSphere endpoint Get all virtual machine classes defined in all managed supervisor clusters in a
      particular vSphere instance. vSphere instance is identified by endpoint
      SelfLink Id Performs GET /cmx/api/resources/vsphere/endpoints/{endpointSelfLinkId}/virtual-machine-classes


    :param string p_endpointSelfLinkId: (required in path)
    """

    try:

        hub.log.debug(
            "GET /cmx/api/resources/vsphere/endpoints/{endpointSelfLinkId}/virtual-machine-classes"
        )

        api = VSphereEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_virtual_machine_classes(
            endpoint_self_link_id=p_endpointSelfLinkId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking VSphereEndpointsApi.get_virtual_machine_classes: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
