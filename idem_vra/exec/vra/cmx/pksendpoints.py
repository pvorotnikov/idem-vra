from idem_vra.client.vra_cmx_lib.api import PKSEndpointsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create2(hub, ctx, **kwargs):
    """Performs POST /cmx/api/resources/pks/endpoints


    :param string agent: (optional in body)
    :param string apiEndpoint: (optional in body)
    :param string caCertificate: (optional in body)
    :param string dcId: (optional in body)
    :param string description: (optional in body)
    :param boolean directConnection: (optional in body)
    :param string documentSelfLink: (optional in body)
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param string orgId: (optional in body)
    :param string password: (optional in body)
    :param string uaaEndpoint: (optional in body)
    :param string username: (optional in body)
    """

    try:

        hub.log.debug("POST /cmx/api/resources/pks/endpoints")

        api = PKSEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        body = {}

        if "agent" in kwargs:
            hub.log.debug(f"Got kwarg 'agent' = {kwargs['agent']}")
            body["agent"] = kwargs.get("agent")
            del kwargs["agent"]
        if "apiEndpoint" in kwargs:
            hub.log.debug(f"Got kwarg 'apiEndpoint' = {kwargs['apiEndpoint']}")
            body["apiEndpoint"] = kwargs.get("apiEndpoint")
            del kwargs["apiEndpoint"]
        if "caCertificate" in kwargs:
            hub.log.debug(f"Got kwarg 'caCertificate' = {kwargs['caCertificate']}")
            body["caCertificate"] = kwargs.get("caCertificate")
            del kwargs["caCertificate"]
        if "dcId" in kwargs:
            hub.log.debug(f"Got kwarg 'dcId' = {kwargs['dcId']}")
            body["dcId"] = kwargs.get("dcId")
            del kwargs["dcId"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "directConnection" in kwargs:
            hub.log.debug(
                f"Got kwarg 'directConnection' = {kwargs['directConnection']}"
            )
            body["directConnection"] = kwargs.get("directConnection")
            del kwargs["directConnection"]
        if "documentSelfLink" in kwargs:
            hub.log.debug(
                f"Got kwarg 'documentSelfLink' = {kwargs['documentSelfLink']}"
            )
            body["documentSelfLink"] = kwargs.get("documentSelfLink")
            del kwargs["documentSelfLink"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "password" in kwargs:
            hub.log.debug(f"Got kwarg 'password' = {kwargs['password']}")
            body["password"] = kwargs.get("password")
            del kwargs["password"]
        if "uaaEndpoint" in kwargs:
            hub.log.debug(f"Got kwarg 'uaaEndpoint' = {kwargs['uaaEndpoint']}")
            body["uaaEndpoint"] = kwargs.get("uaaEndpoint")
            del kwargs["uaaEndpoint"]
        if "username" in kwargs:
            hub.log.debug(f"Got kwarg 'username' = {kwargs['username']}")
            body["username"] = kwargs.get("username")
            del kwargs["username"]

        ret = api.create2(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PKSEndpointsApi.create2: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_cluster(hub, ctx, p_id, **kwargs):
    """Create a K8S cluster Create a K8S cluster on PKS endpoint specified by endpoint id Performs POST /cmx/api/resources/pks/endpoints/{id}/clusters


    :param string p_id: (required in path)
    :param string hostnameAddress: (optional in body)
    :param string ipAddress: (optional in body)
    :param array kubernetes_master_ips: (optional in body)
    :param string last_action: (optional in body)
    :param string last_action_description: (optional in body)
    :param string last_action_state: (optional in body)
    :param string name: (optional in body)
    :param object parameters: (optional in body)
    :param string plan_name: (optional in body)
    :param string uuid: (optional in body)
    """

    try:

        hub.log.debug("POST /cmx/api/resources/pks/endpoints/{id}/clusters")

        api = PKSEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        body = {}

        if "hostnameAddress" in kwargs:
            hub.log.debug(f"Got kwarg 'hostnameAddress' = {kwargs['hostnameAddress']}")
            body["hostnameAddress"] = kwargs.get("hostnameAddress")
            del kwargs["hostnameAddress"]
        if "ipAddress" in kwargs:
            hub.log.debug(f"Got kwarg 'ipAddress' = {kwargs['ipAddress']}")
            body["ipAddress"] = kwargs.get("ipAddress")
            del kwargs["ipAddress"]
        if "kubernetes_master_ips" in kwargs:
            hub.log.debug(
                f"Got kwarg 'kubernetes_master_ips' = {kwargs['kubernetes_master_ips']}"
            )
            body["kubernetes_master_ips"] = kwargs.get("kubernetes_master_ips")
            del kwargs["kubernetes_master_ips"]
        if "last_action" in kwargs:
            hub.log.debug(f"Got kwarg 'last_action' = {kwargs['last_action']}")
            body["last_action"] = kwargs.get("last_action")
            del kwargs["last_action"]
        if "last_action_description" in kwargs:
            hub.log.debug(
                f"Got kwarg 'last_action_description' = {kwargs['last_action_description']}"
            )
            body["last_action_description"] = kwargs.get("last_action_description")
            del kwargs["last_action_description"]
        if "last_action_state" in kwargs:
            hub.log.debug(
                f"Got kwarg 'last_action_state' = {kwargs['last_action_state']}"
            )
            body["last_action_state"] = kwargs.get("last_action_state")
            del kwargs["last_action_state"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "parameters" in kwargs:
            hub.log.debug(f"Got kwarg 'parameters' = {kwargs['parameters']}")
            body["parameters"] = kwargs.get("parameters")
            del kwargs["parameters"]
        if "plan_name" in kwargs:
            hub.log.debug(f"Got kwarg 'plan_name' = {kwargs['plan_name']}")
            body["plan_name"] = kwargs.get("plan_name")
            del kwargs["plan_name"]
        if "uuid" in kwargs:
            hub.log.debug(f"Got kwarg 'uuid' = {kwargs['uuid']}")
            body["uuid"] = kwargs.get("uuid")
            del kwargs["uuid"]

        ret = api.create_cluster(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PKSEndpointsApi.create_cluster: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def destroy_cluster(hub, ctx, p_id, p_clusterId, **kwargs):
    """Destroy a K8S cluster on a specific PKS endpoint Destroy and unregister a K8S cluster on PKS endpoint Performs DELETE /cmx/api/resources/pks/endpoints/{id}/clusters/{clusterId}


    :param string p_id: (required in path)
    :param string p_clusterId: (required in path)
    """

    try:

        hub.log.debug(
            "DELETE /cmx/api/resources/pks/endpoints/{id}/clusters/{clusterId}"
        )

        api = PKSEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.destroy_cluster(id=p_id, cluster_id=p_clusterId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PKSEndpointsApi.destroy_cluster: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_clusters(hub, ctx, p_id, **kwargs):
    """Get all K8S clusters for a PKS endpoint Get all K8S clusters for a PKS endpoint by provided PKS endpoint id Performs GET /cmx/api/resources/pks/endpoints/{id}/clusters


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("GET /cmx/api/resources/pks/endpoints/{id}/clusters")

        api = PKSEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_clusters(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PKSEndpointsApi.get_clusters: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_plans(hub, ctx, p_id, **kwargs):
    """Get supported plans for a PKS endpoint Get supported plans by providing PKS endpoint id Performs GET /cmx/api/resources/pks/endpoints/{id}/plans


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("GET /cmx/api/resources/pks/endpoints/{id}/plans")

        api = PKSEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_plans(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PKSEndpointsApi.get_plans: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list4(hub, ctx, q_pageable, **kwargs):
    """Performs GET /cmx/api/resources/pks/endpoints


    :param None q_pageable: (required in query)
    """

    try:

        hub.log.debug("GET /cmx/api/resources/pks/endpoints")

        api = PKSEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.list4(pageable=q_pageable, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking PKSEndpointsApi.list4: {err}")
        return ExecReturn(result=False, comment=str(err))


async def update_cluster(hub, ctx, p_id, p_clusterId, **kwargs):
    """Update a K8S cluster on a specific endpoint id Update a K8S cluster on a PKS endpoint identified by id Performs PUT /cmx/api/resources/pks/endpoints/{id}/clusters/{clusterId}


    :param string p_id: (required in path)
    :param string p_clusterId: (required in path)
    :param string hostnameAddress: (optional in body)
    :param string ipAddress: (optional in body)
    :param array kubernetes_master_ips: (optional in body)
    :param string last_action: (optional in body)
    :param string last_action_description: (optional in body)
    :param string last_action_state: (optional in body)
    :param string name: (optional in body)
    :param object parameters: (optional in body)
    :param string plan_name: (optional in body)
    :param string uuid: (optional in body)
    """

    try:

        hub.log.debug("PUT /cmx/api/resources/pks/endpoints/{id}/clusters/{clusterId}")

        api = PKSEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        body = {}

        if "hostnameAddress" in kwargs:
            hub.log.debug(f"Got kwarg 'hostnameAddress' = {kwargs['hostnameAddress']}")
            body["hostnameAddress"] = kwargs.get("hostnameAddress")
            del kwargs["hostnameAddress"]
        if "ipAddress" in kwargs:
            hub.log.debug(f"Got kwarg 'ipAddress' = {kwargs['ipAddress']}")
            body["ipAddress"] = kwargs.get("ipAddress")
            del kwargs["ipAddress"]
        if "kubernetes_master_ips" in kwargs:
            hub.log.debug(
                f"Got kwarg 'kubernetes_master_ips' = {kwargs['kubernetes_master_ips']}"
            )
            body["kubernetes_master_ips"] = kwargs.get("kubernetes_master_ips")
            del kwargs["kubernetes_master_ips"]
        if "last_action" in kwargs:
            hub.log.debug(f"Got kwarg 'last_action' = {kwargs['last_action']}")
            body["last_action"] = kwargs.get("last_action")
            del kwargs["last_action"]
        if "last_action_description" in kwargs:
            hub.log.debug(
                f"Got kwarg 'last_action_description' = {kwargs['last_action_description']}"
            )
            body["last_action_description"] = kwargs.get("last_action_description")
            del kwargs["last_action_description"]
        if "last_action_state" in kwargs:
            hub.log.debug(
                f"Got kwarg 'last_action_state' = {kwargs['last_action_state']}"
            )
            body["last_action_state"] = kwargs.get("last_action_state")
            del kwargs["last_action_state"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "parameters" in kwargs:
            hub.log.debug(f"Got kwarg 'parameters' = {kwargs['parameters']}")
            body["parameters"] = kwargs.get("parameters")
            del kwargs["parameters"]
        if "plan_name" in kwargs:
            hub.log.debug(f"Got kwarg 'plan_name' = {kwargs['plan_name']}")
            body["plan_name"] = kwargs.get("plan_name")
            del kwargs["plan_name"]
        if "uuid" in kwargs:
            hub.log.debug(f"Got kwarg 'uuid' = {kwargs['uuid']}")
            body["uuid"] = kwargs.get("uuid")
            del kwargs["uuid"]

        ret = api.update_cluster(body, id=p_id, cluster_id=p_clusterId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PKSEndpointsApi.update_cluster: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
