from idem_vra.client.vra_form_lib.api import FormDefinitionApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_form(hub, ctx, name, status, **kwargs):
    """Create form definition Create form definition. Performs POST /form-service/api/forms


    :param string name: (required in body)
    :param string status: (required in body)
    :param string tenant: (optional in body)
    :param string id: (optional in body)
    :param string form: (optional in body)
    :param string styles: (optional in body)
    :param string sourceType: (optional in body)
    :param string sourceId: (optional in body)
    :param string type: (optional in body)
    :param string createdDate: (optional in body)
    :param string modifiedDate: (optional in body)
    :param string formFormat: (optional in body)
    :param string providerRef: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/forms")

        api = FormDefinitionApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        body = {}
        body["name"] = name
        body["status"] = status

        if "tenant" in kwargs:
            hub.log.debug(f"Got kwarg 'tenant' = {kwargs['tenant']}")
            body["tenant"] = kwargs.get("tenant")
            del kwargs["tenant"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "form" in kwargs:
            hub.log.debug(f"Got kwarg 'form' = {kwargs['form']}")
            body["form"] = kwargs.get("form")
            del kwargs["form"]
        if "styles" in kwargs:
            hub.log.debug(f"Got kwarg 'styles' = {kwargs['styles']}")
            body["styles"] = kwargs.get("styles")
            del kwargs["styles"]
        if "sourceType" in kwargs:
            hub.log.debug(f"Got kwarg 'sourceType' = {kwargs['sourceType']}")
            body["sourceType"] = kwargs.get("sourceType")
            del kwargs["sourceType"]
        if "sourceId" in kwargs:
            hub.log.debug(f"Got kwarg 'sourceId' = {kwargs['sourceId']}")
            body["sourceId"] = kwargs.get("sourceId")
            del kwargs["sourceId"]
        if "type" in kwargs:
            hub.log.debug(f"Got kwarg 'type' = {kwargs['type']}")
            body["type"] = kwargs.get("type")
            del kwargs["type"]
        if "createdDate" in kwargs:
            hub.log.debug(f"Got kwarg 'createdDate' = {kwargs['createdDate']}")
            body["createdDate"] = kwargs.get("createdDate")
            del kwargs["createdDate"]
        if "modifiedDate" in kwargs:
            hub.log.debug(f"Got kwarg 'modifiedDate' = {kwargs['modifiedDate']}")
            body["modifiedDate"] = kwargs.get("modifiedDate")
            del kwargs["modifiedDate"]
        if "formFormat" in kwargs:
            hub.log.debug(f"Got kwarg 'formFormat' = {kwargs['formFormat']}")
            body["formFormat"] = kwargs.get("formFormat")
            del kwargs["formFormat"]
        if "providerRef" in kwargs:
            hub.log.debug(f"Got kwarg 'providerRef' = {kwargs['providerRef']}")
            body["providerRef"] = kwargs.get("providerRef")
            del kwargs["providerRef"]

        ret = api.create_form(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormDefinitionApi.create_form: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_form1(hub, ctx, q_sourceType, q_sourceId, q_formType, **kwargs):
    """Delete form by source Delete the form with the specified sourceType, sourceId and formType. Performs DELETE /form-service/api/forms/deleteBySourceAndType


    :param string q_sourceType: (required in query) The form source type. It can be com.vmw.vro.workflow or
      resource.action, or com.vmw.blueprint.version.
    :param string q_sourceId: (required in query) The form source ID
    :param string q_formType: (required in query) The form type. It can be requestForm.
    """

    try:

        hub.log.debug("DELETE /form-service/api/forms/deleteBySourceAndType")

        api = FormDefinitionApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.delete_form1(
            source_type=q_sourceType,
            source_id=q_sourceId,
            form_type=q_formType,
            **kwargs,
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormDefinitionApi.delete_form1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_form(hub, ctx, p_id, **kwargs):
    """Delete form by ID Delete the form with the specified ID. Performs DELETE /form-service/api/forms/{id}


    :param string p_id: (required in path) Form ID
    """

    try:

        hub.log.debug("DELETE /form-service/api/forms/{id}")

        api = FormDefinitionApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.delete_form(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormDefinitionApi.delete_form: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_form1(hub, ctx, p_ref, **kwargs):
    """Get form by providerRef Retrieve the form definition of a form with the specified provider reference. Performs GET /form-service/api/forms/provider/{ref}


    :param string p_ref: (required in path) Provider Reference
    """

    try:

        hub.log.debug("GET /form-service/api/forms/provider/{ref}")

        api = FormDefinitionApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.get_form1(ref=p_ref, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormDefinitionApi.get_form1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_form(hub, ctx, p_id, **kwargs):
    """Get form by ID Retrieve the Form definition of a form with the specified ID. Performs GET /form-service/api/forms/{id}


    :param string p_id: (required in path) Form identifier
    :param string formFormat: (optional in query) Form definition format
    """

    try:

        hub.log.debug("GET /form-service/api/forms/{id}")

        api = FormDefinitionApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.get_form(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormDefinitionApi.get_form: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def search_by_source_and_type(
    hub, ctx, q_sourceType, q_sourceId, q_formType, **kwargs
):
    """Search form by source Search for form definition by source type, source ID and form type. Performs GET /form-service/api/forms/fetchBySourceAndType


    :param string q_sourceType: (required in query) The form source type. It can be com.vmw.vro.workflow or
      resource.action.
    :param string q_sourceId: (required in query) The form source ID
    :param string q_formType: (required in query) The form type. It can be requestForm.
    :param string formFormat: (optional in query) Form definition format
    """

    try:

        hub.log.debug("GET /form-service/api/forms/fetchBySourceAndType")

        api = FormDefinitionApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.search_by_source_and_type(
            source_type=q_sourceType,
            source_id=q_sourceId,
            form_type=q_formType,
            **kwargs,
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormDefinitionApi.search_by_source_and_type: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def search_form_definitions(hub, ctx, **kwargs):
    """Search form and form versions by term Search for form definitions and form versions with the specified term. Performs GET /form-service/api/forms/search


    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param None pageable: (optional in query)
    :param string term: (optional in query) Search term
    """

    try:

        hub.log.debug("GET /form-service/api/forms/search")

        api = FormDefinitionApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.search_form_definitions(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormDefinitionApi.search_form_definitions: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def search_form_status(hub, ctx, **kwargs):
    """Fetch form status Retrieve statuses of forms specified by reference IDs. Performs POST /form-service/api/forms/fetchStatus"""

    try:

        hub.log.debug("POST /form-service/api/forms/fetchStatus")

        api = FormDefinitionApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.search_form_status(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormDefinitionApi.search_form_status: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
