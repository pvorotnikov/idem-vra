from idem_vra.client.vra_form_lib.api import FormRendererApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def execute_external_action(hub, ctx, **kwargs):
    """Execute an external action to get a value (for the UI)  Performs POST /form-service/api/forms/renderer/external-value


    :param string projectId: (optional in query) The projectId of the project chosen for the request.
    :param string sourceId: (optional in query)
    :param string uri: (optional in body)
    :param string dataSource: (optional in body)
    :param array parameters: (optional in body)
    :param Any fieldType: (optional in body)
    :param integer requestId: (optional in body)
    :param object contextParameters: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/forms/renderer/external-value")

        api = FormRendererApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        body = {}

        if "uri" in kwargs:
            hub.log.debug(f"Got kwarg 'uri' = {kwargs['uri']}")
            body["uri"] = kwargs.get("uri")
            del kwargs["uri"]
        if "dataSource" in kwargs:
            hub.log.debug(f"Got kwarg 'dataSource' = {kwargs['dataSource']}")
            body["dataSource"] = kwargs.get("dataSource")
            del kwargs["dataSource"]
        if "parameters" in kwargs:
            hub.log.debug(f"Got kwarg 'parameters' = {kwargs['parameters']}")
            body["parameters"] = kwargs.get("parameters")
            del kwargs["parameters"]
        if "fieldType" in kwargs:
            hub.log.debug(f"Got kwarg 'fieldType' = {kwargs['fieldType']}")
            body["fieldType"] = kwargs.get("fieldType")
            del kwargs["fieldType"]
        if "requestId" in kwargs:
            hub.log.debug(f"Got kwarg 'requestId' = {kwargs['requestId']}")
            body["requestId"] = kwargs.get("requestId")
            del kwargs["requestId"]
        if "contextParameters" in kwargs:
            hub.log.debug(
                f"Got kwarg 'contextParameters' = {kwargs['contextParameters']}"
            )
            body["contextParameters"] = kwargs.get("contextParameters")
            del kwargs["contextParameters"]

        ret = api.execute_external_action(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormRendererApi.execute_external_action: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def execute_external_actions(hub, ctx, **kwargs):
    """Execute multiple external actions to get a list of values (for the UI)  Performs POST /form-service/api/forms/renderer/external-values


    :param string projectId: (optional in query) The projectId of the project chosen for the request.
    :param string sourceId: (optional in query)
    """

    try:

        hub.log.debug("POST /form-service/api/forms/renderer/external-values")

        api = FormRendererApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.execute_external_actions(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormRendererApi.execute_external_actions: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def fetch_form(hub, ctx, **kwargs):
    """Generate request form for a given library schema.  Performs POST /form-service/api/forms/renderer/request


    :param string sourceType: (optional in query) The request source type
    :param string sourceId: (optional in query) The request source id
    :param string formType: (optional in query) The form type
    :param string type: (optional in body)
    :param boolean encrypted: (optional in body)
    :param boolean additionalProperties: (optional in body)
    :param string title: (optional in body)
    :param string description: (optional in body)
    :param boolean writeOnly: (optional in body)
    :param boolean readOnly: (optional in body)
    :param array allOf: (optional in body)
    :param array anyOf: (optional in body)
    :param array oneOf: (optional in body)
    :param Any not: (optional in body)
    :param Any items: (optional in body)
    :param boolean uniqueItems: (optional in body)
    :param integer maxItems: (optional in body)
    :param integer minItems: (optional in body)
    :param number maximum: (optional in body)
    :param number exclusiveMaximum: (optional in body)
    :param number minimum: (optional in body)
    :param number exclusiveMinimum: (optional in body)
    :param object properties: (optional in body)
    :param array required: (optional in body)
    :param integer maxProperties: (optional in body)
    :param integer minProperties: (optional in body)
    :param object patternProperties: (optional in body)
    :param integer maxLength: (optional in body)
    :param integer minLength: (optional in body)
    :param string pattern: (optional in body)
    :param string format: (optional in body)
    :param string formatMinimum: (optional in body)
    :param string formatMaximum: (optional in body)
    :param array enum: (optional in body)
    :param Any const: (optional in body)
    :param Any default: (optional in body)
    :param string $ref: (optional in body)
    :param string $data: (optional in body)
    :param string $dynamicDefault: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/forms/renderer/request")

        api = FormRendererApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        body = {}

        if "type" in kwargs:
            hub.log.debug(f"Got kwarg 'type' = {kwargs['type']}")
            body["type"] = kwargs.get("type")
            del kwargs["type"]
        if "encrypted" in kwargs:
            hub.log.debug(f"Got kwarg 'encrypted' = {kwargs['encrypted']}")
            body["encrypted"] = kwargs.get("encrypted")
            del kwargs["encrypted"]
        if "additionalProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'additionalProperties' = {kwargs['additionalProperties']}"
            )
            body["additionalProperties"] = kwargs.get("additionalProperties")
            del kwargs["additionalProperties"]
        if "title" in kwargs:
            hub.log.debug(f"Got kwarg 'title' = {kwargs['title']}")
            body["title"] = kwargs.get("title")
            del kwargs["title"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "writeOnly" in kwargs:
            hub.log.debug(f"Got kwarg 'writeOnly' = {kwargs['writeOnly']}")
            body["writeOnly"] = kwargs.get("writeOnly")
            del kwargs["writeOnly"]
        if "readOnly" in kwargs:
            hub.log.debug(f"Got kwarg 'readOnly' = {kwargs['readOnly']}")
            body["readOnly"] = kwargs.get("readOnly")
            del kwargs["readOnly"]
        if "allOf" in kwargs:
            hub.log.debug(f"Got kwarg 'allOf' = {kwargs['allOf']}")
            body["allOf"] = kwargs.get("allOf")
            del kwargs["allOf"]
        if "anyOf" in kwargs:
            hub.log.debug(f"Got kwarg 'anyOf' = {kwargs['anyOf']}")
            body["anyOf"] = kwargs.get("anyOf")
            del kwargs["anyOf"]
        if "oneOf" in kwargs:
            hub.log.debug(f"Got kwarg 'oneOf' = {kwargs['oneOf']}")
            body["oneOf"] = kwargs.get("oneOf")
            del kwargs["oneOf"]
        if "not" in kwargs:
            hub.log.debug(f"Got kwarg 'not' = {kwargs['not']}")
            body["not"] = kwargs.get("not")
            del kwargs["not"]
        if "items" in kwargs:
            hub.log.debug(f"Got kwarg 'items' = {kwargs['items']}")
            body["items"] = kwargs.get("items")
            del kwargs["items"]
        if "uniqueItems" in kwargs:
            hub.log.debug(f"Got kwarg 'uniqueItems' = {kwargs['uniqueItems']}")
            body["uniqueItems"] = kwargs.get("uniqueItems")
            del kwargs["uniqueItems"]
        if "maxItems" in kwargs:
            hub.log.debug(f"Got kwarg 'maxItems' = {kwargs['maxItems']}")
            body["maxItems"] = kwargs.get("maxItems")
            del kwargs["maxItems"]
        if "minItems" in kwargs:
            hub.log.debug(f"Got kwarg 'minItems' = {kwargs['minItems']}")
            body["minItems"] = kwargs.get("minItems")
            del kwargs["minItems"]
        if "maximum" in kwargs:
            hub.log.debug(f"Got kwarg 'maximum' = {kwargs['maximum']}")
            body["maximum"] = kwargs.get("maximum")
            del kwargs["maximum"]
        if "exclusiveMaximum" in kwargs:
            hub.log.debug(
                f"Got kwarg 'exclusiveMaximum' = {kwargs['exclusiveMaximum']}"
            )
            body["exclusiveMaximum"] = kwargs.get("exclusiveMaximum")
            del kwargs["exclusiveMaximum"]
        if "minimum" in kwargs:
            hub.log.debug(f"Got kwarg 'minimum' = {kwargs['minimum']}")
            body["minimum"] = kwargs.get("minimum")
            del kwargs["minimum"]
        if "exclusiveMinimum" in kwargs:
            hub.log.debug(
                f"Got kwarg 'exclusiveMinimum' = {kwargs['exclusiveMinimum']}"
            )
            body["exclusiveMinimum"] = kwargs.get("exclusiveMinimum")
            del kwargs["exclusiveMinimum"]
        if "properties" in kwargs:
            hub.log.debug(f"Got kwarg 'properties' = {kwargs['properties']}")
            body["properties"] = kwargs.get("properties")
            del kwargs["properties"]
        if "required" in kwargs:
            hub.log.debug(f"Got kwarg 'required' = {kwargs['required']}")
            body["required"] = kwargs.get("required")
            del kwargs["required"]
        if "maxProperties" in kwargs:
            hub.log.debug(f"Got kwarg 'maxProperties' = {kwargs['maxProperties']}")
            body["maxProperties"] = kwargs.get("maxProperties")
            del kwargs["maxProperties"]
        if "minProperties" in kwargs:
            hub.log.debug(f"Got kwarg 'minProperties' = {kwargs['minProperties']}")
            body["minProperties"] = kwargs.get("minProperties")
            del kwargs["minProperties"]
        if "patternProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'patternProperties' = {kwargs['patternProperties']}"
            )
            body["patternProperties"] = kwargs.get("patternProperties")
            del kwargs["patternProperties"]
        if "maxLength" in kwargs:
            hub.log.debug(f"Got kwarg 'maxLength' = {kwargs['maxLength']}")
            body["maxLength"] = kwargs.get("maxLength")
            del kwargs["maxLength"]
        if "minLength" in kwargs:
            hub.log.debug(f"Got kwarg 'minLength' = {kwargs['minLength']}")
            body["minLength"] = kwargs.get("minLength")
            del kwargs["minLength"]
        if "pattern" in kwargs:
            hub.log.debug(f"Got kwarg 'pattern' = {kwargs['pattern']}")
            body["pattern"] = kwargs.get("pattern")
            del kwargs["pattern"]
        if "format" in kwargs:
            hub.log.debug(f"Got kwarg 'format' = {kwargs['format']}")
            body["format"] = kwargs.get("format")
            del kwargs["format"]
        if "formatMinimum" in kwargs:
            hub.log.debug(f"Got kwarg 'formatMinimum' = {kwargs['formatMinimum']}")
            body["formatMinimum"] = kwargs.get("formatMinimum")
            del kwargs["formatMinimum"]
        if "formatMaximum" in kwargs:
            hub.log.debug(f"Got kwarg 'formatMaximum' = {kwargs['formatMaximum']}")
            body["formatMaximum"] = kwargs.get("formatMaximum")
            del kwargs["formatMaximum"]
        if "enum" in kwargs:
            hub.log.debug(f"Got kwarg 'enum' = {kwargs['enum']}")
            body["enum"] = kwargs.get("enum")
            del kwargs["enum"]
        if "const" in kwargs:
            hub.log.debug(f"Got kwarg 'const' = {kwargs['const']}")
            body["const"] = kwargs.get("const")
            del kwargs["const"]
        if "default" in kwargs:
            hub.log.debug(f"Got kwarg 'default' = {kwargs['default']}")
            body["default"] = kwargs.get("default")
            del kwargs["default"]
        if "$ref" in kwargs:
            hub.log.debug(f"Got kwarg '$ref' = {kwargs['$ref']}")
            body["$ref"] = kwargs.get("$ref")
            del kwargs["$ref"]
        if "$data" in kwargs:
            hub.log.debug(f"Got kwarg '$data' = {kwargs['$data']}")
            body["$data"] = kwargs.get("$data")
            del kwargs["$data"]
        if "$dynamicDefault" in kwargs:
            hub.log.debug(f"Got kwarg '$dynamicDefault' = {kwargs['$dynamicDefault']}")
            body["$dynamicDefault"] = kwargs.get("$dynamicDefault")
            del kwargs["$dynamicDefault"]

        ret = api.fetch_form(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormRendererApi.fetch_form: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def fetch_model(hub, ctx, **kwargs):
    """Generate request form model for a given library schema.  Performs POST /form-service/api/forms/renderer/model


    :param string sourceType: (optional in query) The request source type
    :param string sourceId: (optional in query) The request source id
    :param string formType: (optional in query) The form type
    :param string formId: (optional in query) The request form id, used to find a form from a provider
    :param string resourceId: (optional in query) Resource id
    :param string deploymentId: (optional in query) Deployment id
    :param string type: (optional in body)
    :param boolean encrypted: (optional in body)
    :param boolean additionalProperties: (optional in body)
    :param string title: (optional in body)
    :param string description: (optional in body)
    :param boolean writeOnly: (optional in body)
    :param boolean readOnly: (optional in body)
    :param array allOf: (optional in body)
    :param array anyOf: (optional in body)
    :param array oneOf: (optional in body)
    :param Any not: (optional in body)
    :param Any items: (optional in body)
    :param boolean uniqueItems: (optional in body)
    :param integer maxItems: (optional in body)
    :param integer minItems: (optional in body)
    :param number maximum: (optional in body)
    :param number exclusiveMaximum: (optional in body)
    :param number minimum: (optional in body)
    :param number exclusiveMinimum: (optional in body)
    :param object properties: (optional in body)
    :param array required: (optional in body)
    :param integer maxProperties: (optional in body)
    :param integer minProperties: (optional in body)
    :param object patternProperties: (optional in body)
    :param integer maxLength: (optional in body)
    :param integer minLength: (optional in body)
    :param string pattern: (optional in body)
    :param string format: (optional in body)
    :param string formatMinimum: (optional in body)
    :param string formatMaximum: (optional in body)
    :param array enum: (optional in body)
    :param Any const: (optional in body)
    :param Any default: (optional in body)
    :param string $ref: (optional in body)
    :param string $data: (optional in body)
    :param string $dynamicDefault: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/forms/renderer/model")

        api = FormRendererApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        body = {}

        if "type" in kwargs:
            hub.log.debug(f"Got kwarg 'type' = {kwargs['type']}")
            body["type"] = kwargs.get("type")
            del kwargs["type"]
        if "encrypted" in kwargs:
            hub.log.debug(f"Got kwarg 'encrypted' = {kwargs['encrypted']}")
            body["encrypted"] = kwargs.get("encrypted")
            del kwargs["encrypted"]
        if "additionalProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'additionalProperties' = {kwargs['additionalProperties']}"
            )
            body["additionalProperties"] = kwargs.get("additionalProperties")
            del kwargs["additionalProperties"]
        if "title" in kwargs:
            hub.log.debug(f"Got kwarg 'title' = {kwargs['title']}")
            body["title"] = kwargs.get("title")
            del kwargs["title"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "writeOnly" in kwargs:
            hub.log.debug(f"Got kwarg 'writeOnly' = {kwargs['writeOnly']}")
            body["writeOnly"] = kwargs.get("writeOnly")
            del kwargs["writeOnly"]
        if "readOnly" in kwargs:
            hub.log.debug(f"Got kwarg 'readOnly' = {kwargs['readOnly']}")
            body["readOnly"] = kwargs.get("readOnly")
            del kwargs["readOnly"]
        if "allOf" in kwargs:
            hub.log.debug(f"Got kwarg 'allOf' = {kwargs['allOf']}")
            body["allOf"] = kwargs.get("allOf")
            del kwargs["allOf"]
        if "anyOf" in kwargs:
            hub.log.debug(f"Got kwarg 'anyOf' = {kwargs['anyOf']}")
            body["anyOf"] = kwargs.get("anyOf")
            del kwargs["anyOf"]
        if "oneOf" in kwargs:
            hub.log.debug(f"Got kwarg 'oneOf' = {kwargs['oneOf']}")
            body["oneOf"] = kwargs.get("oneOf")
            del kwargs["oneOf"]
        if "not" in kwargs:
            hub.log.debug(f"Got kwarg 'not' = {kwargs['not']}")
            body["not"] = kwargs.get("not")
            del kwargs["not"]
        if "items" in kwargs:
            hub.log.debug(f"Got kwarg 'items' = {kwargs['items']}")
            body["items"] = kwargs.get("items")
            del kwargs["items"]
        if "uniqueItems" in kwargs:
            hub.log.debug(f"Got kwarg 'uniqueItems' = {kwargs['uniqueItems']}")
            body["uniqueItems"] = kwargs.get("uniqueItems")
            del kwargs["uniqueItems"]
        if "maxItems" in kwargs:
            hub.log.debug(f"Got kwarg 'maxItems' = {kwargs['maxItems']}")
            body["maxItems"] = kwargs.get("maxItems")
            del kwargs["maxItems"]
        if "minItems" in kwargs:
            hub.log.debug(f"Got kwarg 'minItems' = {kwargs['minItems']}")
            body["minItems"] = kwargs.get("minItems")
            del kwargs["minItems"]
        if "maximum" in kwargs:
            hub.log.debug(f"Got kwarg 'maximum' = {kwargs['maximum']}")
            body["maximum"] = kwargs.get("maximum")
            del kwargs["maximum"]
        if "exclusiveMaximum" in kwargs:
            hub.log.debug(
                f"Got kwarg 'exclusiveMaximum' = {kwargs['exclusiveMaximum']}"
            )
            body["exclusiveMaximum"] = kwargs.get("exclusiveMaximum")
            del kwargs["exclusiveMaximum"]
        if "minimum" in kwargs:
            hub.log.debug(f"Got kwarg 'minimum' = {kwargs['minimum']}")
            body["minimum"] = kwargs.get("minimum")
            del kwargs["minimum"]
        if "exclusiveMinimum" in kwargs:
            hub.log.debug(
                f"Got kwarg 'exclusiveMinimum' = {kwargs['exclusiveMinimum']}"
            )
            body["exclusiveMinimum"] = kwargs.get("exclusiveMinimum")
            del kwargs["exclusiveMinimum"]
        if "properties" in kwargs:
            hub.log.debug(f"Got kwarg 'properties' = {kwargs['properties']}")
            body["properties"] = kwargs.get("properties")
            del kwargs["properties"]
        if "required" in kwargs:
            hub.log.debug(f"Got kwarg 'required' = {kwargs['required']}")
            body["required"] = kwargs.get("required")
            del kwargs["required"]
        if "maxProperties" in kwargs:
            hub.log.debug(f"Got kwarg 'maxProperties' = {kwargs['maxProperties']}")
            body["maxProperties"] = kwargs.get("maxProperties")
            del kwargs["maxProperties"]
        if "minProperties" in kwargs:
            hub.log.debug(f"Got kwarg 'minProperties' = {kwargs['minProperties']}")
            body["minProperties"] = kwargs.get("minProperties")
            del kwargs["minProperties"]
        if "patternProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'patternProperties' = {kwargs['patternProperties']}"
            )
            body["patternProperties"] = kwargs.get("patternProperties")
            del kwargs["patternProperties"]
        if "maxLength" in kwargs:
            hub.log.debug(f"Got kwarg 'maxLength' = {kwargs['maxLength']}")
            body["maxLength"] = kwargs.get("maxLength")
            del kwargs["maxLength"]
        if "minLength" in kwargs:
            hub.log.debug(f"Got kwarg 'minLength' = {kwargs['minLength']}")
            body["minLength"] = kwargs.get("minLength")
            del kwargs["minLength"]
        if "pattern" in kwargs:
            hub.log.debug(f"Got kwarg 'pattern' = {kwargs['pattern']}")
            body["pattern"] = kwargs.get("pattern")
            del kwargs["pattern"]
        if "format" in kwargs:
            hub.log.debug(f"Got kwarg 'format' = {kwargs['format']}")
            body["format"] = kwargs.get("format")
            del kwargs["format"]
        if "formatMinimum" in kwargs:
            hub.log.debug(f"Got kwarg 'formatMinimum' = {kwargs['formatMinimum']}")
            body["formatMinimum"] = kwargs.get("formatMinimum")
            del kwargs["formatMinimum"]
        if "formatMaximum" in kwargs:
            hub.log.debug(f"Got kwarg 'formatMaximum' = {kwargs['formatMaximum']}")
            body["formatMaximum"] = kwargs.get("formatMaximum")
            del kwargs["formatMaximum"]
        if "enum" in kwargs:
            hub.log.debug(f"Got kwarg 'enum' = {kwargs['enum']}")
            body["enum"] = kwargs.get("enum")
            del kwargs["enum"]
        if "const" in kwargs:
            hub.log.debug(f"Got kwarg 'const' = {kwargs['const']}")
            body["const"] = kwargs.get("const")
            del kwargs["const"]
        if "default" in kwargs:
            hub.log.debug(f"Got kwarg 'default' = {kwargs['default']}")
            body["default"] = kwargs.get("default")
            del kwargs["default"]
        if "$ref" in kwargs:
            hub.log.debug(f"Got kwarg '$ref' = {kwargs['$ref']}")
            body["$ref"] = kwargs.get("$ref")
            del kwargs["$ref"]
        if "$data" in kwargs:
            hub.log.debug(f"Got kwarg '$data' = {kwargs['$data']}")
            body["$data"] = kwargs.get("$data")
            del kwargs["$data"]
        if "$dynamicDefault" in kwargs:
            hub.log.debug(f"Got kwarg '$dynamicDefault' = {kwargs['$dynamicDefault']}")
            body["$dynamicDefault"] = kwargs.get("$dynamicDefault")
            del kwargs["$dynamicDefault"]

        ret = api.fetch_model(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormRendererApi.fetch_model: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def lookup_external_resource(
    hub, ctx, q_namespace, q_type, q_objectId, q_projectId, **kwargs
):
    """Retrieve external resource object Retrieve external resource object depending on namespace type and id Performs GET /form-service/api/forms/renderer/external-resources


    :param string q_namespace: (required in query) namespace in which the vRO type is placed
    :param string q_type: (required in query) type of the SDK object
    :param string q_objectId: (required in query) unique id of the object
    :param string q_projectId: (required in query) id of current project
    """

    try:

        hub.log.debug("GET /form-service/api/forms/renderer/external-resources")

        api = FormRendererApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.lookup_external_resource(
            namespace=q_namespace,
            type=q_type,
            object_id=q_objectId,
            project_id=q_projectId,
            **kwargs,
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormRendererApi.lookup_external_resource: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def lookup_external_resources(hub, ctx, **kwargs):
    """Retrieve external resource value Retrieve external resource value depending on provided value source type Performs POST /form-service/api/forms/renderer/external-resources


    :param Any item: (optional in body)
    :param string filter: (optional in body)
    :param Any valueSource: (optional in body)
    :param Any valueType: (optional in body)
    :param object context: (optional in body)
    :param string projectId: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/forms/renderer/external-resources")

        api = FormRendererApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        body = {}

        if "item" in kwargs:
            hub.log.debug(f"Got kwarg 'item' = {kwargs['item']}")
            body["item"] = kwargs.get("item")
            del kwargs["item"]
        if "filter" in kwargs:
            hub.log.debug(f"Got kwarg 'filter' = {kwargs['filter']}")
            body["filter"] = kwargs.get("filter")
            del kwargs["filter"]
        if "valueSource" in kwargs:
            hub.log.debug(f"Got kwarg 'valueSource' = {kwargs['valueSource']}")
            body["valueSource"] = kwargs.get("valueSource")
            del kwargs["valueSource"]
        if "valueType" in kwargs:
            hub.log.debug(f"Got kwarg 'valueType' = {kwargs['valueType']}")
            body["valueType"] = kwargs.get("valueType")
            del kwargs["valueType"]
        if "context" in kwargs:
            hub.log.debug(f"Got kwarg 'context' = {kwargs['context']}")
            body["context"] = kwargs.get("context")
            del kwargs["context"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]

        ret = api.lookup_external_resources(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormRendererApi.lookup_external_resources: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def perform_external_validation(hub, ctx, **kwargs):
    """Perform external validations for a form (for the UI)  Performs POST /form-service/api/forms/renderer/external-validation


    :param string projectId: (optional in query) The projectId of the project chosen for the request.
    :param string sourceId: (optional in query)
    :param array externalValidations: (optional in body)
    :param object context: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/forms/renderer/external-validation")

        api = FormRendererApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        body = {}

        if "externalValidations" in kwargs:
            hub.log.debug(
                f"Got kwarg 'externalValidations' = {kwargs['externalValidations']}"
            )
            body["externalValidations"] = kwargs.get("externalValidations")
            del kwargs["externalValidations"]
        if "context" in kwargs:
            hub.log.debug(f"Got kwarg 'context' = {kwargs['context']}")
            body["context"] = kwargs.get("context")
            del kwargs["context"]

        ret = api.perform_external_validation(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormRendererApi.perform_external_validation: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
