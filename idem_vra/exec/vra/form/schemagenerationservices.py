from idem_vra.client.vra_form_lib.api import SchemaGenerationServicesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def generate(hub, ctx, id, type, **kwargs):
    """Generate schema for runnable item. Generate a ProviderSchemaImpl for the provided RunnableItem. The schema can be
      used to create a custom resource type. Performs POST /form-service/api/custom/schema-generation


    :param string id: (required in body)
    :param string type: (required in body)
    :param string name: (optional in body)
    :param string description: (optional in body)
    :param string projectId: (optional in body)
    :param array inputParameters: (optional in body)
    :param array outputParameters: (optional in body)
    :param string endpointLink: (optional in body)
    :param array inputBindings: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/custom/schema-generation")

        api = SchemaGenerationServicesApi(
            hub.clients["idem_vra.client.vra_form_lib.api"]
        )

        body = {}
        body["id"] = id
        body["type"] = type

        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "inputParameters" in kwargs:
            hub.log.debug(f"Got kwarg 'inputParameters' = {kwargs['inputParameters']}")
            body["inputParameters"] = kwargs.get("inputParameters")
            del kwargs["inputParameters"]
        if "outputParameters" in kwargs:
            hub.log.debug(
                f"Got kwarg 'outputParameters' = {kwargs['outputParameters']}"
            )
            body["outputParameters"] = kwargs.get("outputParameters")
            del kwargs["outputParameters"]
        if "endpointLink" in kwargs:
            hub.log.debug(f"Got kwarg 'endpointLink' = {kwargs['endpointLink']}")
            body["endpointLink"] = kwargs.get("endpointLink")
            del kwargs["endpointLink"]
        if "inputBindings" in kwargs:
            hub.log.debug(f"Got kwarg 'inputBindings' = {kwargs['inputBindings']}")
            body["inputBindings"] = kwargs.get("inputBindings")
            del kwargs["inputBindings"]

        ret = api.generate(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SchemaGenerationServicesApi.generate: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_form_layout_schema(hub, ctx, id, type, **kwargs):
    """Get form layout schema for runnable item. Get the layout schema for the provided runnable item, given that it exists on
      the specified endpoint Performs POST /form-service/api/custom/schema-generation/form


    :param string id: (required in body)
    :param string type: (required in body)
    :param string name: (optional in body)
    :param string description: (optional in body)
    :param string projectId: (optional in body)
    :param array inputParameters: (optional in body)
    :param array outputParameters: (optional in body)
    :param string endpointLink: (optional in body)
    :param array inputBindings: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/custom/schema-generation/form")

        api = SchemaGenerationServicesApi(
            hub.clients["idem_vra.client.vra_form_lib.api"]
        )

        body = {}
        body["id"] = id
        body["type"] = type

        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "inputParameters" in kwargs:
            hub.log.debug(f"Got kwarg 'inputParameters' = {kwargs['inputParameters']}")
            body["inputParameters"] = kwargs.get("inputParameters")
            del kwargs["inputParameters"]
        if "outputParameters" in kwargs:
            hub.log.debug(
                f"Got kwarg 'outputParameters' = {kwargs['outputParameters']}"
            )
            body["outputParameters"] = kwargs.get("outputParameters")
            del kwargs["outputParameters"]
        if "endpointLink" in kwargs:
            hub.log.debug(f"Got kwarg 'endpointLink' = {kwargs['endpointLink']}")
            body["endpointLink"] = kwargs.get("endpointLink")
            del kwargs["endpointLink"]
        if "inputBindings" in kwargs:
            hub.log.debug(f"Got kwarg 'inputBindings' = {kwargs['inputBindings']}")
            body["inputBindings"] = kwargs.get("inputBindings")
            del kwargs["inputBindings"]

        ret = api.get_form_layout_schema(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SchemaGenerationServicesApi.get_form_layout_schema: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_json_object_schema(hub, ctx, q_externalType, id, type, **kwargs):
    """Get json object schema for runnable item. Generate a ProviderSchemaImpl for the provided RunnableItem. The schema can be
      used to create a custom resource type. Performs POST /form-service/api/custom/schema-generation/json


    :param string q_externalType: (required in query) External type
    :param string id: (required in body)
    :param string type: (required in body)
    :param string name: (optional in body)
    :param string description: (optional in body)
    :param string projectId: (optional in body)
    :param array inputParameters: (optional in body)
    :param array outputParameters: (optional in body)
    :param string endpointLink: (optional in body)
    :param array inputBindings: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/custom/schema-generation/json")

        api = SchemaGenerationServicesApi(
            hub.clients["idem_vra.client.vra_form_lib.api"]
        )

        body = {}
        body["id"] = id
        body["type"] = type

        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "inputParameters" in kwargs:
            hub.log.debug(f"Got kwarg 'inputParameters' = {kwargs['inputParameters']}")
            body["inputParameters"] = kwargs.get("inputParameters")
            del kwargs["inputParameters"]
        if "outputParameters" in kwargs:
            hub.log.debug(
                f"Got kwarg 'outputParameters' = {kwargs['outputParameters']}"
            )
            body["outputParameters"] = kwargs.get("outputParameters")
            del kwargs["outputParameters"]
        if "endpointLink" in kwargs:
            hub.log.debug(f"Got kwarg 'endpointLink' = {kwargs['endpointLink']}")
            body["endpointLink"] = kwargs.get("endpointLink")
            del kwargs["endpointLink"]
        if "inputBindings" in kwargs:
            hub.log.debug(f"Got kwarg 'inputBindings' = {kwargs['inputBindings']}")
            body["inputBindings"] = kwargs.get("inputBindings")
            del kwargs["inputBindings"]

        ret = api.get_json_object_schema(body, external_type=q_externalType, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SchemaGenerationServicesApi.get_json_object_schema: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
