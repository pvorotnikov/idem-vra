from idem_vra.client.vra_identity_lib.api import CheckIDTokenControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def check_id_token(hub, ctx, q_Authorization, **kwargs):
    """Check ID Token Display the content of the given ID token with expanded claims if the token is
      valid.<br/>If the token is invalid or expired, an error will be
      returned.<br/>This endpoint should be used to expand the overflow claims in the
      ID token, if any (like the group_names and group_ids claim).<br/>Even
      though this endpoint can also be used to validate an ID token, it is expected
      that the client validates an ID token locally instead.
      Access Policy
      Role  Access
      Anonymous    Performs GET /oauth/check_id_token


    :param string q_Authorization: (required in header) ID token in Bearer Authentication format i.e. Bearer XXX where XXX
      is the content of an ID token
    """

    try:

        hub.log.debug("GET /oauth/check_id_token")

        api = CheckIDTokenControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.check_id_token(authorization=q_Authorization, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CheckIDTokenControllerApi.check_id_token: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
