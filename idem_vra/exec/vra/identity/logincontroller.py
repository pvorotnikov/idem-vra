from idem_vra.client.vra_identity_lib.api import LoginControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def login_oauth(hub, ctx, grant_type, state, **kwargs):
    """Get Access token. An alias of the AuthenticationControllers get access token method.
      Access Policy
      Role  Access
      Anonymous    Performs POST /csp/gateway/am/api/login/oauth


    :param string grant_type: (required in body) The type of authorization to be performed.
    :param string state: (required in body) A transparent state of the request.
    :param string authorization: (optional in header)
    :param string refresh_token: (optional in body) The refresh token when grant_type</code> is set to
      refresh_token</code>
    :param string code: (optional in body) The authorization code when grant_type</code> is set to
      authorization_code</code>
    :param string redirect_uri: (optional in body) The URI to which a redirect will be performed upon successful
      authorization.
    :param string client_id: (optional in body) The client ID when grant_type</code> is set to
      client_credentials</code>. Will be ignored if the
      Authorization</code> header is set.
    :param string client_secret: (optional in body) The client secret when grant_type</code> is set to
      client_credentials</code>. Will be ignored if the
      Authorization</code> header is set.
    :param string scope: (optional in body) Currently not supported. Present for CSP compatibility.
    :param string orgId: (optional in body) When grant_type</code> is set to client_credentials</code>
      if this parameter is set the issued token will be limited to the
      specified organization.
    """

    try:

        hub.log.debug("POST /csp/gateway/am/api/login/oauth")

        api = LoginControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        body = {}
        body["grant_type"] = grant_type
        body["state"] = state

        if "refresh_token" in kwargs:
            hub.log.debug(f"Got kwarg 'refresh_token' = {kwargs['refresh_token']}")
            body["refresh_token"] = kwargs.get("refresh_token")
            del kwargs["refresh_token"]
        if "code" in kwargs:
            hub.log.debug(f"Got kwarg 'code' = {kwargs['code']}")
            body["code"] = kwargs.get("code")
            del kwargs["code"]
        if "redirect_uri" in kwargs:
            hub.log.debug(f"Got kwarg 'redirect_uri' = {kwargs['redirect_uri']}")
            body["redirect_uri"] = kwargs.get("redirect_uri")
            del kwargs["redirect_uri"]
        if "client_id" in kwargs:
            hub.log.debug(f"Got kwarg 'client_id' = {kwargs['client_id']}")
            body["client_id"] = kwargs.get("client_id")
            del kwargs["client_id"]
        if "client_secret" in kwargs:
            hub.log.debug(f"Got kwarg 'client_secret' = {kwargs['client_secret']}")
            body["client_secret"] = kwargs.get("client_secret")
            del kwargs["client_secret"]
        if "scope" in kwargs:
            hub.log.debug(f"Got kwarg 'scope' = {kwargs['scope']}")
            body["scope"] = kwargs.get("scope")
            del kwargs["scope"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]

        ret = api.login_oauth(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking LoginControllerApi.login_oauth: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def login(hub, ctx, username, password, **kwargs):
    """Login. Performs login.
      Access Policy
      Role  Access
      Anonymous    Performs POST /csp/gateway/am/api/login


    :param string username: (required in body) The username.
    :param string password: (required in body) The password.
    :param string access_token: (optional in query) When this parameter is present, the response will contain refresh
      token.
    :param string domain: (optional in body) The users domain.
    :param string scope: (optional in body) Scope of the issued token.
    """

    try:

        hub.log.debug("POST /csp/gateway/am/api/login")

        api = LoginControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        body = {}
        body["username"] = username
        body["password"] = password

        if "domain" in kwargs:
            hub.log.debug(f"Got kwarg 'domain' = {kwargs['domain']}")
            body["domain"] = kwargs.get("domain")
            del kwargs["domain"]
        if "scope" in kwargs:
            hub.log.debug(f"Got kwarg 'scope' = {kwargs['scope']}")
            body["scope"] = kwargs.get("scope")
            del kwargs["scope"]

        ret = api.login(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking LoginControllerApi.login: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
