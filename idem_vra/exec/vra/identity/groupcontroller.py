from idem_vra.client.vra_identity_lib.api import GroupControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def search_groups(hub, ctx, q_searchTerm, **kwargs):
    """Search for a group. In this case the PagedResponse.results field will contain Group</code>
      object. See the Group</code> model for reference.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/groups/search


    :param string q_searchTerm: (required in query) The search term, uses contains filter on the group displayName
      attribute.
    :param string idpId: (optional in query) This parameter is included for CSP compatibility and its value is
      ignored.
    :param integer pageStart: (optional in query) The number of results per page.
    :param integer pageLimit: (optional in query) The index of the first element of the page. One based.
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/groups/search")

        api = GroupControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        ret = api.search_groups(search_term=q_searchTerm, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking GroupControllerApi.search_groups: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
