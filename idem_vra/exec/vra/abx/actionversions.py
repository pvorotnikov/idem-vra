from idem_vra.client.vra_abx_lib.api import ActionVersionsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_action_version(hub, ctx, p_id, q_projectId, **kwargs):
    """Create a version for an action Creates a new version for the specified action Performs POST /abx/api/resources/actions/{id}/versions


    :param string p_id: (required in path) ID of the action
    :param string q_projectId: (required in query) Project ID of action (required for non-system actions)
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param integer createdMillis: (optional in body)
    :param string orgId: (optional in body)
    :param string description: (optional in body)
    :param string actionId: (optional in body)
    :param string projectId: (optional in body)
    :param string createdBy: (optional in body)
    :param boolean released: (optional in body)
    :param Any action: (optional in body)
    :param string contentId: (optional in body)
    :param string gitCommitId: (optional in body)
    """

    try:

        hub.log.debug("POST /abx/api/resources/actions/{id}/versions")

        api = ActionVersionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "createdMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
            body["createdMillis"] = kwargs.get("createdMillis")
            del kwargs["createdMillis"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "actionId" in kwargs:
            hub.log.debug(f"Got kwarg 'actionId' = {kwargs['actionId']}")
            body["actionId"] = kwargs.get("actionId")
            del kwargs["actionId"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "createdBy" in kwargs:
            hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
            body["createdBy"] = kwargs.get("createdBy")
            del kwargs["createdBy"]
        if "released" in kwargs:
            hub.log.debug(f"Got kwarg 'released' = {kwargs['released']}")
            body["released"] = kwargs.get("released")
            del kwargs["released"]
        if "action" in kwargs:
            hub.log.debug(f"Got kwarg 'action' = {kwargs['action']}")
            body["action"] = kwargs.get("action")
            del kwargs["action"]
        if "contentId" in kwargs:
            hub.log.debug(f"Got kwarg 'contentId' = {kwargs['contentId']}")
            body["contentId"] = kwargs.get("contentId")
            del kwargs["contentId"]
        if "gitCommitId" in kwargs:
            hub.log.debug(f"Got kwarg 'gitCommitId' = {kwargs['gitCommitId']}")
            body["gitCommitId"] = kwargs.get("gitCommitId")
            del kwargs["gitCommitId"]

        ret = api.create_action_version(body, id=p_id, project_id=q_projectId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionVersionsApi.create_action_version: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_action_version(hub, ctx, p_id, p_versionId, q_projectId, **kwargs):
    """Delete an action version Deletes an action version Performs DELETE /abx/api/resources/actions/{id}/versions/{versionId}


    :param string p_id: (required in path) ID of the action
    :param string p_versionId: (required in path) ID of the action version
    :param string q_projectId: (required in query) Project ID of action (required for non-system actions)
    """

    try:

        hub.log.debug("DELETE /abx/api/resources/actions/{id}/versions/{versionId}")

        api = ActionVersionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.delete_action_version(
            id=p_id, version_id=p_versionId, project_id=q_projectId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionVersionsApi.delete_action_version: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_versions(hub, ctx, p_id, q_projectId, **kwargs):
    """Fetch all versions of an action Retrieves all created versions for an action Performs GET /abx/api/resources/actions/{id}/versions


    :param string p_id: (required in path) ID of the action
    :param string q_projectId: (required in query) Project ID of action (required for non-system actions)
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /abx/api/resources/actions/{id}/versions")

        api = ActionVersionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get_all_versions(id=p_id, project_id=q_projectId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionVersionsApi.get_all_versions: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def release_action_version(hub, ctx, p_id, q_projectId, version, **kwargs):
    """Mark an action version as released Marks an exisiting version of an action as released Performs PUT /abx/api/resources/actions/{id}/release


    :param string p_id: (required in path) ID of the action
    :param string q_projectId: (required in query) Project ID of action (required for non-system actions)
    :param string version: (required in body)
    """

    try:

        hub.log.debug("PUT /abx/api/resources/actions/{id}/release")

        api = ActionVersionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}
        body["version"] = version

        ret = api.release_action_version(
            body, id=p_id, project_id=q_projectId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionVersionsApi.release_action_version: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def restore_action_version(hub, ctx, p_id, p_versionId, q_projectId, **kwargs):
    """Restore the action state based on a specified version Change the current action state to the state specified in the version Performs PUT /abx/api/resources/actions/{id}/versions/{versionId}/restore


    :param string p_id: (required in path) ID of the action
    :param string p_versionId: (required in path) ID of the action version
    :param string q_projectId: (required in query) Project ID of action (required for non-system actions)
    """

    try:

        hub.log.debug(
            "PUT /abx/api/resources/actions/{id}/versions/{versionId}/restore"
        )

        api = ActionVersionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.restore_action_version(
            id=p_id, version_id=p_versionId, project_id=q_projectId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionVersionsApi.restore_action_version: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def unrelease_action_version(hub, ctx, p_id, q_projectId, **kwargs):
    """Mark an action version as not released Marks an actions released version as not released Performs DELETE /abx/api/resources/actions/{id}/release


    :param string p_id: (required in path) ID of the action
    :param string q_projectId: (required in query) Project ID of action (required for non-system actions)
    """

    try:

        hub.log.debug("DELETE /abx/api/resources/actions/{id}/release")

        api = ActionVersionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.unrelease_action_version(id=p_id, project_id=q_projectId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionVersionsApi.unrelease_action_version: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
