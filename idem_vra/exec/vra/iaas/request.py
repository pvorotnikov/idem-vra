from idem_vra.client.vra_iaas_lib.api import RequestApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def delete_request(hub, ctx, p_id, **kwargs):
    """Delete Request Delete a single Request Performs DELETE /iaas/api/request-tracker/{id}


    :param string p_id: (required in path) The ID of the request.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/request-tracker/{id}")

        api = RequestApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_request(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking RequestApi.delete_request: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_request_tracker(hub, ctx, p_id, **kwargs):
    """Get request tracker Get request tracker with a given id Performs GET /iaas/api/request-tracker/{id}


    :param string p_id: (required in path) The ID of the request.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/request-tracker/{id}")

        api = RequestApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_request_tracker(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking RequestApi.get_request_tracker: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_request_trackers(hub, ctx, **kwargs):
    """Get request tracker Get all request trackers Performs GET /iaas/api/request-tracker


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/request-tracker")

        api = RequestApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_request_trackers(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking RequestApi.get_request_trackers: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
