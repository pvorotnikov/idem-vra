from idem_vra.client.vra_iaas_lib.api import FabricImagesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_fabric_image(hub, ctx, p_id, **kwargs):
    """Get fabric image Get fabric image with a given id Performs GET /iaas/api/fabric-images/{id}


    :param string p_id: (required in path) The ID of the image.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string select: (optional in query) Select a subset of properties to include in the response.
    """

    try:

        hub.log.debug("GET /iaas/api/fabric-images/{id}")

        api = FabricImagesApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_fabric_image(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FabricImagesApi.get_fabric_image: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_fabric_images(hub, ctx, **kwargs):
    """Get fabric images Get all fabric images Performs GET /iaas/api/fabric-images


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    try:

        hub.log.debug("GET /iaas/api/fabric-images")

        api = FabricImagesApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_fabric_images(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FabricImagesApi.get_fabric_images: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
