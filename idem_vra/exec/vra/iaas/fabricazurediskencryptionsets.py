from idem_vra.client.vra_iaas_lib.api import FabricAzureDiskEncryptionSetsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_azure_disk_encryption_sets(hub, ctx, q_regionId, **kwargs):
    """Get Azure disk encryption sets Get all Azure disk encryption sets Performs GET /iaas/api/fabric-azure-disk-encryption-sets


    :param string q_regionId: (required in query) Region id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/fabric-azure-disk-encryption-sets")

        api = FabricAzureDiskEncryptionSetsApi(
            hub.clients["idem_vra.client.vra_iaas_lib.api"]
        )
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_azure_disk_encryption_sets(region_id=q_regionId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FabricAzureDiskEncryptionSetsApi.get_azure_disk_encryption_sets: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
