from idem_vra.client.vra_iaas_lib.api import FlavorsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_flavors(hub, ctx, **kwargs):
    """Get flavors Get all flavors defined in FlavorProfile Performs GET /iaas/api/flavors


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/flavors")

        api = FlavorsApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_flavors(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FlavorsApi.get_flavors: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
