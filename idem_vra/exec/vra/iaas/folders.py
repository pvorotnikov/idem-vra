from idem_vra.client.vra_iaas_lib.api import FoldersApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_folders(hub, ctx, **kwargs):
    """Get folders Get all folders within the current organization Performs GET /iaas/api/folders


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    :param string cloudAccountId: (optional in query) The ID of a vcenter cloud account.
    :param string externalRegionId: (optional in query) The external unique identifier of the region associated with the
      vcenter cloud account.
    """

    try:

        hub.log.debug("GET /iaas/api/folders")

        api = FoldersApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_folders(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FoldersApi.get_folders: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
