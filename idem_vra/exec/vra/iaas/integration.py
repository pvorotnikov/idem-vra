from idem_vra.client.vra_iaas_lib.api import IntegrationApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_integration_async(
    hub, ctx, name, integrationType, integrationProperties, **kwargs
):
    """Create an integration Create an integration in the current organization asynchronously Performs POST /iaas/api/integrations


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string integrationType: (required in body) Integration type
    :param object integrationProperties: (required in body) Integration specific properties supplied in as name value pairs
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string validateOnly: (optional in query) Only validate provided Integration Specification. Integration will not
      be created.
    :param string description: (optional in body) A human-friendly description.
    :param string privateKeyId: (optional in body) Access key id or username to be used to authenticate with the
      integration
    :param string privateKey: (optional in body) Secret access key or password to be used to authenticate with the
      integration
    :param array associatedCloudAccountIds: (optional in body) Cloud accounts to associate with this integration
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend the
      Integration.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Integration
    :param Any certificateInfo: (optional in body)
    """

    try:

        hub.log.debug("POST /iaas/api/integrations")

        api = IntegrationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["integrationType"] = integrationType
        body["integrationProperties"] = integrationProperties

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "privateKeyId" in kwargs:
            hub.log.debug(f"Got kwarg 'privateKeyId' = {kwargs['privateKeyId']}")
            body["privateKeyId"] = kwargs.get("privateKeyId")
            del kwargs["privateKeyId"]
        if "privateKey" in kwargs:
            hub.log.debug(f"Got kwarg 'privateKey' = {kwargs['privateKey']}")
            body["privateKey"] = kwargs.get("privateKey")
            del kwargs["privateKey"]
        if "associatedCloudAccountIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'associatedCloudAccountIds' = {kwargs['associatedCloudAccountIds']}"
            )
            body["associatedCloudAccountIds"] = kwargs.get("associatedCloudAccountIds")
            del kwargs["associatedCloudAccountIds"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]

        ret = api.create_integration_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking IntegrationApi.create_integration_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_integration(hub, ctx, p_id, **kwargs):
    """Delete an integration Delete an integration with a given id asynchronously Performs DELETE /iaas/api/integrations/{id}


    :param string p_id: (required in path) The ID of the Integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/integrations/{id}")

        api = IntegrationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_integration(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking IntegrationApi.delete_integration: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_integration(hub, ctx, p_id, **kwargs):
    """Get an integration Get an integration with a given id Performs GET /iaas/api/integrations/{id}


    :param string p_id: (required in path) The ID of the Integration
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string select: (optional in query) Select a subset of properties to include in the response.
    """

    try:

        hub.log.debug("GET /iaas/api/integrations/{id}")

        api = IntegrationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_integration(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking IntegrationApi.get_integration: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_integrations(hub, ctx, **kwargs):
    """Get integrations Get all integrations within the current organization Performs GET /iaas/api/integrations


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    try:

        hub.log.debug("GET /iaas/api/integrations")

        api = IntegrationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_integrations(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking IntegrationApi.get_integrations: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_integration_async(
    hub, ctx, p_id, name, integrationProperties, **kwargs
):
    """Update an integration Update a single integration asynchronously Performs PATCH /iaas/api/integrations/{id}


    :param string p_id: (required in path) The ID of the integration
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param object integrationProperties: (required in body) Integration specific properties supplied in as name value pairs
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param string privateKeyId: (optional in body) Access key id or username to be used to authenticate with the
      integration
    :param string privateKey: (optional in body) Secret access key or password to be used to authenticate with the
      integration
    :param array associatedCloudAccountIds: (optional in body) Cloud accounts to associate with this integration
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend the
      Integration.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Integration
    :param Any certificateInfo: (optional in body)
    """

    try:

        hub.log.debug("PATCH /iaas/api/integrations/{id}")

        api = IntegrationApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["integrationProperties"] = integrationProperties

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "privateKeyId" in kwargs:
            hub.log.debug(f"Got kwarg 'privateKeyId' = {kwargs['privateKeyId']}")
            body["privateKeyId"] = kwargs.get("privateKeyId")
            del kwargs["privateKeyId"]
        if "privateKey" in kwargs:
            hub.log.debug(f"Got kwarg 'privateKey' = {kwargs['privateKey']}")
            body["privateKey"] = kwargs.get("privateKey")
            del kwargs["privateKey"]
        if "associatedCloudAccountIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'associatedCloudAccountIds' = {kwargs['associatedCloudAccountIds']}"
            )
            body["associatedCloudAccountIds"] = kwargs.get("associatedCloudAccountIds")
            del kwargs["associatedCloudAccountIds"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]

        ret = api.update_integration_async(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking IntegrationApi.update_integration_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
