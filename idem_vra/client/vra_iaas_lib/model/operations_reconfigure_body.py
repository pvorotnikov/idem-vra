# coding: utf-8

"""
    VMware Aria Automation Assembler IaaS API

    IAAS API is a RESTful service, which allows users to execute provisioning related UI actions via an API.  This page describes the RESTful APIs for IAAS API. The APIs facilitate CRUD operations on the various resources and entities used throughout VMware Aria Automation Assembler(Cloud Accounts, Cloud zones, Computes, Mappings, etc.) and allows operations on them (creating a cloud account, patching a machine, creating image progile, etc.).  The APIs that list collections of resources also support OData like implementation. Below query params can be used across different IAAS API endpoints  Example: 1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.     ```/iaas/api/cloud-accounts?$orderby=name%20desc```  2. `$top` -  number of records you want to get.     ```/iaas/api/cloud-accounts?$top=20```  3. `$skip` -  number of records you want to skip.      ```/iaas/api/cloud-accounts?$skip=10```  4. `$select` - select a subset of properties to include in the response.        ```/project-service/api/projects?$top=10&$skip=2```  5. `$filter` -  filter the results by a specified predicate expression. Operators: eq, ne, and, or.\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'ABC*'``` - name starts with 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC*'``` - name contains 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC'``` - name ends with 'ABC'\\     /iaas/api/projects and /iaas/api/deployments support different format for partial match:\\     ```/iaas/api/projects?$filter=startswith(name, 'ABC')``` - name starts with 'ABC'\\     ```/iaas/api/projects?$filter=substringof('ABC', name``` - name contains 'ABC'\\     ```/iaas/api/projects?$filter=endswith(name, 'ABC')``` - name ends with 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20ne%20'example-cloud-account'%20or%20customProperties.isExternal%20eq%20'false'```  6. `$count` -  flag which when specified, regardless of the assigned value, shows the total number of records. If the collection has a filter it shows the number of records matching the filter.\\     ```/iaas/api/cloud-accounts?$count=true```   # noqa: E501

    OpenAPI spec version: 2021-07-15
    Contact: sales@vmware.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class OperationsReconfigureBody(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'name': 'str',
        'project_id': 'str',
        'deployment_id': 'str',
        'custom_properties': 'dict(str, str)',
        'description': 'str',
        'rules': 'list[IaasapisecuritygroupsidoperationsreconfigureRules]',
        'tags': 'list[IaasapistorageprofilesidTags]'
    }

    attribute_map = {
        'name': 'name',
        'project_id': 'projectId',
        'deployment_id': 'deploymentId',
        'custom_properties': 'customProperties',
        'description': 'description',
        'rules': 'rules',
        'tags': 'tags'
    }

    def __init__(self, name=None, project_id=None, deployment_id=None, custom_properties=None, description=None, rules=None, tags=None):  # noqa: E501
        """OperationsReconfigureBody - a model defined in Swagger"""  # noqa: E501
        self._name = None
        self._project_id = None
        self._deployment_id = None
        self._custom_properties = None
        self._description = None
        self._rules = None
        self._tags = None
        self.discriminator = None
        self.name = name
        self.project_id = project_id
        if deployment_id is not None:
            self.deployment_id = deployment_id
        if custom_properties is not None:
            self.custom_properties = custom_properties
        if description is not None:
            self.description = description
        if rules is not None:
            self.rules = rules
        if tags is not None:
            self.tags = tags

    @property
    def name(self):
        """Gets the name of this OperationsReconfigureBody.  # noqa: E501

        A human-friendly name used as an identifier in APIs that support this option.  # noqa: E501

        :return: The name of this OperationsReconfigureBody.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this OperationsReconfigureBody.

        A human-friendly name used as an identifier in APIs that support this option.  # noqa: E501

        :param name: The name of this OperationsReconfigureBody.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def project_id(self):
        """Gets the project_id of this OperationsReconfigureBody.  # noqa: E501

        The id of the project the current user belongs to.  # noqa: E501

        :return: The project_id of this OperationsReconfigureBody.  # noqa: E501
        :rtype: str
        """
        return self._project_id

    @project_id.setter
    def project_id(self, project_id):
        """Sets the project_id of this OperationsReconfigureBody.

        The id of the project the current user belongs to.  # noqa: E501

        :param project_id: The project_id of this OperationsReconfigureBody.  # noqa: E501
        :type: str
        """
        if project_id is None:
            raise ValueError("Invalid value for `project_id`, must not be `None`")  # noqa: E501

        self._project_id = project_id

    @property
    def deployment_id(self):
        """Gets the deployment_id of this OperationsReconfigureBody.  # noqa: E501

        The id of the deployment that is associated with this resource  # noqa: E501

        :return: The deployment_id of this OperationsReconfigureBody.  # noqa: E501
        :rtype: str
        """
        return self._deployment_id

    @deployment_id.setter
    def deployment_id(self, deployment_id):
        """Sets the deployment_id of this OperationsReconfigureBody.

        The id of the deployment that is associated with this resource  # noqa: E501

        :param deployment_id: The deployment_id of this OperationsReconfigureBody.  # noqa: E501
        :type: str
        """

        self._deployment_id = deployment_id

    @property
    def custom_properties(self):
        """Gets the custom_properties of this OperationsReconfigureBody.  # noqa: E501

        Additional custom properties that may be used to extend this resource.  # noqa: E501

        :return: The custom_properties of this OperationsReconfigureBody.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._custom_properties

    @custom_properties.setter
    def custom_properties(self, custom_properties):
        """Sets the custom_properties of this OperationsReconfigureBody.

        Additional custom properties that may be used to extend this resource.  # noqa: E501

        :param custom_properties: The custom_properties of this OperationsReconfigureBody.  # noqa: E501
        :type: dict(str, str)
        """

        self._custom_properties = custom_properties

    @property
    def description(self):
        """Gets the description of this OperationsReconfigureBody.  # noqa: E501

        A human-friendly description.  # noqa: E501

        :return: The description of this OperationsReconfigureBody.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this OperationsReconfigureBody.

        A human-friendly description.  # noqa: E501

        :param description: The description of this OperationsReconfigureBody.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def rules(self):
        """Gets the rules of this OperationsReconfigureBody.  # noqa: E501

        List of security rules.  # noqa: E501

        :return: The rules of this OperationsReconfigureBody.  # noqa: E501
        :rtype: list[IaasapisecuritygroupsidoperationsreconfigureRules]
        """
        return self._rules

    @rules.setter
    def rules(self, rules):
        """Sets the rules of this OperationsReconfigureBody.

        List of security rules.  # noqa: E501

        :param rules: The rules of this OperationsReconfigureBody.  # noqa: E501
        :type: list[IaasapisecuritygroupsidoperationsreconfigureRules]
        """

        self._rules = rules

    @property
    def tags(self):
        """Gets the tags of this OperationsReconfigureBody.  # noqa: E501

        A set of tag keys and optional values that should be set on any resource that is produced from this specification.  # noqa: E501

        :return: The tags of this OperationsReconfigureBody.  # noqa: E501
        :rtype: list[IaasapistorageprofilesidTags]
        """
        return self._tags

    @tags.setter
    def tags(self, tags):
        """Sets the tags of this OperationsReconfigureBody.

        A set of tag keys and optional values that should be set on any resource that is produced from this specification.  # noqa: E501

        :param tags: The tags of this OperationsReconfigureBody.  # noqa: E501
        :type: list[IaasapistorageprofilesidTags]
        """

        self._tags = tags

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(OperationsReconfigureBody, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, OperationsReconfigureBody):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
