# coding: utf-8

"""
    Identity Service API

    A list of identity, account and service management APIs.  # noqa: E501

    OpenAPI spec version: 1.6.5
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class ExpandedTypedUserGroups(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'display_name': 'str',
        'domain': 'str',
        'users_count': 'int',
        'group_type': 'str'
    }

    attribute_map = {
        'id': 'id',
        'display_name': 'displayName',
        'domain': 'domain',
        'users_count': 'usersCount',
        'group_type': 'groupType'
    }

    def __init__(self, id=None, display_name=None, domain=None, users_count=None, group_type=None):  # noqa: E501
        """ExpandedTypedUserGroups - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._display_name = None
        self._domain = None
        self._users_count = None
        self._group_type = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if display_name is not None:
            self.display_name = display_name
        if domain is not None:
            self.domain = domain
        if users_count is not None:
            self.users_count = users_count
        if group_type is not None:
            self.group_type = group_type

    @property
    def id(self):
        """Gets the id of this ExpandedTypedUserGroups.  # noqa: E501

        The group ID.  # noqa: E501

        :return: The id of this ExpandedTypedUserGroups.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ExpandedTypedUserGroups.

        The group ID.  # noqa: E501

        :param id: The id of this ExpandedTypedUserGroups.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def display_name(self):
        """Gets the display_name of this ExpandedTypedUserGroups.  # noqa: E501

        The group display name.  # noqa: E501

        :return: The display_name of this ExpandedTypedUserGroups.  # noqa: E501
        :rtype: str
        """
        return self._display_name

    @display_name.setter
    def display_name(self, display_name):
        """Sets the display_name of this ExpandedTypedUserGroups.

        The group display name.  # noqa: E501

        :param display_name: The display_name of this ExpandedTypedUserGroups.  # noqa: E501
        :type: str
        """

        self._display_name = display_name

    @property
    def domain(self):
        """Gets the domain of this ExpandedTypedUserGroups.  # noqa: E501

        The group domain.  # noqa: E501

        :return: The domain of this ExpandedTypedUserGroups.  # noqa: E501
        :rtype: str
        """
        return self._domain

    @domain.setter
    def domain(self, domain):
        """Sets the domain of this ExpandedTypedUserGroups.

        The group domain.  # noqa: E501

        :param domain: The domain of this ExpandedTypedUserGroups.  # noqa: E501
        :type: str
        """

        self._domain = domain

    @property
    def users_count(self):
        """Gets the users_count of this ExpandedTypedUserGroups.  # noqa: E501

        The number of users members of this group.  # noqa: E501

        :return: The users_count of this ExpandedTypedUserGroups.  # noqa: E501
        :rtype: int
        """
        return self._users_count

    @users_count.setter
    def users_count(self, users_count):
        """Sets the users_count of this ExpandedTypedUserGroups.

        The number of users members of this group.  # noqa: E501

        :param users_count: The users_count of this ExpandedTypedUserGroups.  # noqa: E501
        :type: int
        """

        self._users_count = users_count

    @property
    def group_type(self):
        """Gets the group_type of this ExpandedTypedUserGroups.  # noqa: E501

        USER_GROUP is an organization specific custom group, AD_GROUP is a federated domain group  # noqa: E501

        :return: The group_type of this ExpandedTypedUserGroups.  # noqa: E501
        :rtype: str
        """
        return self._group_type

    @group_type.setter
    def group_type(self, group_type):
        """Sets the group_type of this ExpandedTypedUserGroups.

        USER_GROUP is an organization specific custom group, AD_GROUP is a federated domain group  # noqa: E501

        :param group_type: The group_type of this ExpandedTypedUserGroups.  # noqa: E501
        :type: str
        """
        allowed_values = ["USER_GROUP", "AD_GROUP", "DOMAIN_GROUP"]  # noqa: E501
        if group_type not in allowed_values:
            raise ValueError(
                "Invalid value for `group_type` ({0}), must be one of {1}"  # noqa: E501
                .format(group_type, allowed_values)
            )

        self._group_type = group_type

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ExpandedTypedUserGroups, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ExpandedTypedUserGroups):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
