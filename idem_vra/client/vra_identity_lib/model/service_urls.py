# coding: utf-8

"""
    Identity Service API

    A list of identity, account and service management APIs.  # noqa: E501

    OpenAPI spec version: 1.6.5
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class ServiceUrls(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'offer_configuration': 'str',
        'service_home': 'str',
        'request_access': 'str'
    }

    attribute_map = {
        'offer_configuration': 'offerConfiguration',
        'service_home': 'serviceHome',
        'request_access': 'requestAccess'
    }

    def __init__(self, offer_configuration=None, service_home=None, request_access=None):  # noqa: E501
        """ServiceUrls - a model defined in Swagger"""  # noqa: E501
        self._offer_configuration = None
        self._service_home = None
        self._request_access = None
        self.discriminator = None
        if offer_configuration is not None:
            self.offer_configuration = offer_configuration
        if service_home is not None:
            self.service_home = service_home
        if request_access is not None:
            self.request_access = request_access

    @property
    def offer_configuration(self):
        """Gets the offer_configuration of this ServiceUrls.  # noqa: E501


        :return: The offer_configuration of this ServiceUrls.  # noqa: E501
        :rtype: str
        """
        return self._offer_configuration

    @offer_configuration.setter
    def offer_configuration(self, offer_configuration):
        """Sets the offer_configuration of this ServiceUrls.


        :param offer_configuration: The offer_configuration of this ServiceUrls.  # noqa: E501
        :type: str
        """

        self._offer_configuration = offer_configuration

    @property
    def service_home(self):
        """Gets the service_home of this ServiceUrls.  # noqa: E501


        :return: The service_home of this ServiceUrls.  # noqa: E501
        :rtype: str
        """
        return self._service_home

    @service_home.setter
    def service_home(self, service_home):
        """Sets the service_home of this ServiceUrls.


        :param service_home: The service_home of this ServiceUrls.  # noqa: E501
        :type: str
        """

        self._service_home = service_home

    @property
    def request_access(self):
        """Gets the request_access of this ServiceUrls.  # noqa: E501


        :return: The request_access of this ServiceUrls.  # noqa: E501
        :rtype: str
        """
        return self._request_access

    @request_access.setter
    def request_access(self, request_access):
        """Sets the request_access of this ServiceUrls.


        :param request_access: The request_access of this ServiceUrls.  # noqa: E501
        :type: str
        """

        self._request_access = request_access

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ServiceUrls, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ServiceUrls):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
