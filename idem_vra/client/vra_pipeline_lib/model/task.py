# coding: utf-8

"""
    VMware Aria Automation Pipelines APIs

    Aria Automation Pipelines is a continuous integration and continuous delivery (CICD) tool that you use to build Pipelines that model the software release process in your DevOps lifecycle. By creating Pipelines, you build the code infrastructure that delivers your software rapidly and continuously.  This page describes the RESTful APIs for Aria Automation Pipelines. The APIs facilitate CRUD operations on the various resources and entities used throughout Aria Automation Pipelines (Pipelines, Endpoints, Variables, etc.) and allow operations on them (executing a Pipeline, validating an Endpoint connection, etc.).  The APIs that list collections of resources  also support OData like implementation. Below query params can be used across Aria Automation Pipelines entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/codestream/api/endpoints?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/codestream/api/endpoints?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/codestream/api/endpoints?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /codestream/api/endpoints?$filter=startswith(name, 'ABC')     /codestream/api/endpoints?$filter=toupper(name) eq 'ABCD-JENKINS'     /codestream/api/endpoints?$filter=substringof(%27bc%27,tolower(name))     /codestream/api/endpoints?$filter=name eq 'ABCD' and project eq 'demo'   # noqa: E501

    OpenAPI spec version: 2019-10-17
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class Task(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'endpoints': 'dict(str, str)',
        'ignore_failure': 'bool',
        'input': 'object',
        'pre_condition': 'str',
        'tags': 'list[str]',
        'type': 'str'
    }

    attribute_map = {
        'endpoints': 'endpoints',
        'ignore_failure': 'ignoreFailure',
        'input': 'input',
        'pre_condition': 'preCondition',
        'tags': 'tags',
        'type': 'type'
    }

    def __init__(self, endpoints=None, ignore_failure=None, input=None, pre_condition=None, tags=None, type=None):  # noqa: E501
        """Task - a model defined in Swagger"""  # noqa: E501
        self._endpoints = None
        self._ignore_failure = None
        self._input = None
        self._pre_condition = None
        self._tags = None
        self._type = None
        self.discriminator = None
        if endpoints is not None:
            self.endpoints = endpoints
        if ignore_failure is not None:
            self.ignore_failure = ignore_failure
        if input is not None:
            self.input = input
        if pre_condition is not None:
            self.pre_condition = pre_condition
        if tags is not None:
            self.tags = tags
        if type is not None:
            self.type = type

    @property
    def endpoints(self):
        """Gets the endpoints of this Task.  # noqa: E501

        Map representing the Output properties for the Task.  # noqa: E501

        :return: The endpoints of this Task.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._endpoints

    @endpoints.setter
    def endpoints(self, endpoints):
        """Sets the endpoints of this Task.

        Map representing the Output properties for the Task.  # noqa: E501

        :param endpoints: The endpoints of this Task.  # noqa: E501
        :type: dict(str, str)
        """

        self._endpoints = endpoints

    @property
    def ignore_failure(self):
        """Gets the ignore_failure of this Task.  # noqa: E501

        Ignores the failure of the Task execution if set to true, and continues with the Pipeline execution.  # noqa: E501

        :return: The ignore_failure of this Task.  # noqa: E501
        :rtype: bool
        """
        return self._ignore_failure

    @ignore_failure.setter
    def ignore_failure(self, ignore_failure):
        """Sets the ignore_failure of this Task.

        Ignores the failure of the Task execution if set to true, and continues with the Pipeline execution.  # noqa: E501

        :param ignore_failure: The ignore_failure of this Task.  # noqa: E501
        :type: bool
        """

        self._ignore_failure = ignore_failure

    @property
    def input(self):
        """Gets the input of this Task.  # noqa: E501

        Map representing the Input properties for the Task.  # noqa: E501

        :return: The input of this Task.  # noqa: E501
        :rtype: object
        """
        return self._input

    @input.setter
    def input(self, input):
        """Sets the input of this Task.

        Map representing the Input properties for the Task.  # noqa: E501

        :param input: The input of this Task.  # noqa: E501
        :type: object
        """

        self._input = input

    @property
    def pre_condition(self):
        """Gets the pre_condition of this Task.  # noqa: E501

        The Task is executed only if this field evaluates to true.  # noqa: E501

        :return: The pre_condition of this Task.  # noqa: E501
        :rtype: str
        """
        return self._pre_condition

    @pre_condition.setter
    def pre_condition(self, pre_condition):
        """Sets the pre_condition of this Task.

        The Task is executed only if this field evaluates to true.  # noqa: E501

        :param pre_condition: The pre_condition of this Task.  # noqa: E501
        :type: str
        """

        self._pre_condition = pre_condition

    @property
    def tags(self):
        """Gets the tags of this Task.  # noqa: E501

        A set of tag keys and optional values that were set on on the resource.  # noqa: E501

        :return: The tags of this Task.  # noqa: E501
        :rtype: list[str]
        """
        return self._tags

    @tags.setter
    def tags(self, tags):
        """Sets the tags of this Task.

        A set of tag keys and optional values that were set on on the resource.  # noqa: E501

        :param tags: The tags of this Task.  # noqa: E501
        :type: list[str]
        """

        self._tags = tags

    @property
    def type(self):
        """Gets the type of this Task.  # noqa: E501

        Type of the Task.  # noqa: E501

        :return: The type of this Task.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this Task.

        Type of the Task.  # noqa: E501

        :param type: The type of this Task.  # noqa: E501
        :type: str
        """

        self._type = type

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Task, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Task):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
