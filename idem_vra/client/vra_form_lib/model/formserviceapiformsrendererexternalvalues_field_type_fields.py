# coding: utf-8

"""
    Form Service API

    A list of form and Xaas APIs.  This page describes the RESTful APIs for Form Service. The APIs facilitate CRUD operations on the various resources and entities used throughout Form service (Custom Resource , Form Definition etc.) and allow operations on them (creating custom resource type , fetch form status etc.).  Some of the APIs that list collections of resources also support OData like implementation. Below query params can be used across Form and XaaS entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/form-service/api/custom/resource-types?$orderby=name%20DESC```  2. `page`, `size` - page used in conjunction with `size` helps in pagination of resources.      ```/form-service/api/custom/resource-types?page=0&size=20```   # noqa: E501

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class FormserviceapiformsrendererexternalvaluesFieldTypeFields(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'label': 'str',
        'description': 'str',
        'signpost': 'str',
        'type': 'FormSchemaFieldType',
        'step': 'float',
        'default': 'object',
        'value_list': 'object',
        'placeholder': 'str',
        'constraints': 'FormserviceapiformsrendererexternalvaluesFieldTypeConstraints',
        'refresh': 'FormserviceapiformsrendererexternalvaluesFieldTypeRefresh',
        'short_value_name': 'list[str]',
        'id': 'str',
        'value_tree': 'object'
    }

    attribute_map = {
        'label': 'label',
        'description': 'description',
        'signpost': 'signpost',
        'type': 'type',
        'step': 'step',
        'default': 'default',
        'value_list': 'valueList',
        'placeholder': 'placeholder',
        'constraints': 'constraints',
        'refresh': 'refresh',
        'short_value_name': 'shortValueName',
        'id': 'id',
        'value_tree': 'valueTree'
    }

    def __init__(self, label=None, description=None, signpost=None, type=None, step=None, default=None, value_list=None, placeholder=None, constraints=None, refresh=None, short_value_name=None, id=None, value_tree=None):  # noqa: E501
        """FormserviceapiformsrendererexternalvaluesFieldTypeFields - a model defined in Swagger"""  # noqa: E501
        self._label = None
        self._description = None
        self._signpost = None
        self._type = None
        self._step = None
        self._default = None
        self._value_list = None
        self._placeholder = None
        self._constraints = None
        self._refresh = None
        self._short_value_name = None
        self._id = None
        self._value_tree = None
        self.discriminator = None
        if label is not None:
            self.label = label
        if description is not None:
            self.description = description
        if signpost is not None:
            self.signpost = signpost
        if type is not None:
            self.type = type
        if step is not None:
            self.step = step
        if default is not None:
            self.default = default
        if value_list is not None:
            self.value_list = value_list
        if placeholder is not None:
            self.placeholder = placeholder
        if constraints is not None:
            self.constraints = constraints
        if refresh is not None:
            self.refresh = refresh
        if short_value_name is not None:
            self.short_value_name = short_value_name
        if id is not None:
            self.id = id
        if value_tree is not None:
            self.value_tree = value_tree

    @property
    def label(self):
        """Gets the label of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501


        :return: The label of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :rtype: str
        """
        return self._label

    @label.setter
    def label(self, label):
        """Sets the label of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.


        :param label: The label of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :type: str
        """

        self._label = label

    @property
    def description(self):
        """Gets the description of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501


        :return: The description of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.


        :param description: The description of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def signpost(self):
        """Gets the signpost of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501


        :return: The signpost of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :rtype: str
        """
        return self._signpost

    @signpost.setter
    def signpost(self, signpost):
        """Sets the signpost of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.


        :param signpost: The signpost of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :type: str
        """

        self._signpost = signpost

    @property
    def type(self):
        """Gets the type of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501


        :return: The type of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :rtype: FormSchemaFieldType
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.


        :param type: The type of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :type: FormSchemaFieldType
        """

        self._type = type

    @property
    def step(self):
        """Gets the step of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501


        :return: The step of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :rtype: float
        """
        return self._step

    @step.setter
    def step(self, step):
        """Sets the step of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.


        :param step: The step of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :type: float
        """

        self._step = step

    @property
    def default(self):
        """Gets the default of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501


        :return: The default of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :rtype: object
        """
        return self._default

    @default.setter
    def default(self, default):
        """Sets the default of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.


        :param default: The default of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :type: object
        """

        self._default = default

    @property
    def value_list(self):
        """Gets the value_list of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501


        :return: The value_list of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :rtype: object
        """
        return self._value_list

    @value_list.setter
    def value_list(self, value_list):
        """Sets the value_list of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.


        :param value_list: The value_list of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :type: object
        """

        self._value_list = value_list

    @property
    def placeholder(self):
        """Gets the placeholder of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501


        :return: The placeholder of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :rtype: str
        """
        return self._placeholder

    @placeholder.setter
    def placeholder(self, placeholder):
        """Sets the placeholder of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.


        :param placeholder: The placeholder of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :type: str
        """

        self._placeholder = placeholder

    @property
    def constraints(self):
        """Gets the constraints of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501


        :return: The constraints of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :rtype: FormserviceapiformsrendererexternalvaluesFieldTypeConstraints
        """
        return self._constraints

    @constraints.setter
    def constraints(self, constraints):
        """Sets the constraints of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.


        :param constraints: The constraints of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :type: FormserviceapiformsrendererexternalvaluesFieldTypeConstraints
        """

        self._constraints = constraints

    @property
    def refresh(self):
        """Gets the refresh of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501


        :return: The refresh of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :rtype: FormserviceapiformsrendererexternalvaluesFieldTypeRefresh
        """
        return self._refresh

    @refresh.setter
    def refresh(self, refresh):
        """Sets the refresh of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.


        :param refresh: The refresh of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :type: FormserviceapiformsrendererexternalvaluesFieldTypeRefresh
        """

        self._refresh = refresh

    @property
    def short_value_name(self):
        """Gets the short_value_name of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501


        :return: The short_value_name of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :rtype: list[str]
        """
        return self._short_value_name

    @short_value_name.setter
    def short_value_name(self, short_value_name):
        """Sets the short_value_name of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.


        :param short_value_name: The short_value_name of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :type: list[str]
        """

        self._short_value_name = short_value_name

    @property
    def id(self):
        """Gets the id of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501


        :return: The id of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.


        :param id: The id of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def value_tree(self):
        """Gets the value_tree of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501


        :return: The value_tree of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :rtype: object
        """
        return self._value_tree

    @value_tree.setter
    def value_tree(self, value_tree):
        """Sets the value_tree of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.


        :param value_tree: The value_tree of this FormserviceapiformsrendererexternalvaluesFieldTypeFields.  # noqa: E501
        :type: object
        """

        self._value_tree = value_tree

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(FormserviceapiformsrendererexternalvaluesFieldTypeFields, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, FormserviceapiformsrendererexternalvaluesFieldTypeFields):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
