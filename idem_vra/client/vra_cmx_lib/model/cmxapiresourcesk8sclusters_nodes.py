# coding: utf-8

"""
    VMware Aria Automation Assembler / CMX Service API

    When using Kubernetes integration, deploy and manage Kubernetes clusters and namespaces. The APIs that list collections of resources also support OData like implementation. Below query params can be used across CMX entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/cmx/api/resources/tags?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/cmx/api/resources/tags?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/cmx/api/resources/tags?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /cmx/api/resources/tags?$filter=startswith(name, 'K8S')   # noqa: E501

    OpenAPI spec version: 2019-09-12
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class Cmxapiresourcesk8sclustersNodes(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'allocatable_pods': 'int',
        'cpu_cores': 'float',
        'cpu_usage': 'float',
        'created_millis': 'int',
        'description': 'str',
        'id': 'str',
        'memory_usage': 'float',
        'name': 'str',
        'org_id': 'str',
        'total_memory': 'float',
        'updated_millis': 'int'
    }

    attribute_map = {
        'allocatable_pods': 'allocatablePods',
        'cpu_cores': 'cpuCores',
        'cpu_usage': 'cpuUsage',
        'created_millis': 'createdMillis',
        'description': 'description',
        'id': 'id',
        'memory_usage': 'memoryUsage',
        'name': 'name',
        'org_id': 'orgId',
        'total_memory': 'totalMemory',
        'updated_millis': 'updatedMillis'
    }

    def __init__(self, allocatable_pods=None, cpu_cores=None, cpu_usage=None, created_millis=None, description=None, id=None, memory_usage=None, name=None, org_id=None, total_memory=None, updated_millis=None):  # noqa: E501
        """Cmxapiresourcesk8sclustersNodes - a model defined in Swagger"""  # noqa: E501
        self._allocatable_pods = None
        self._cpu_cores = None
        self._cpu_usage = None
        self._created_millis = None
        self._description = None
        self._id = None
        self._memory_usage = None
        self._name = None
        self._org_id = None
        self._total_memory = None
        self._updated_millis = None
        self.discriminator = None
        if allocatable_pods is not None:
            self.allocatable_pods = allocatable_pods
        if cpu_cores is not None:
            self.cpu_cores = cpu_cores
        if cpu_usage is not None:
            self.cpu_usage = cpu_usage
        if created_millis is not None:
            self.created_millis = created_millis
        if description is not None:
            self.description = description
        if id is not None:
            self.id = id
        if memory_usage is not None:
            self.memory_usage = memory_usage
        if name is not None:
            self.name = name
        if org_id is not None:
            self.org_id = org_id
        if total_memory is not None:
            self.total_memory = total_memory
        if updated_millis is not None:
            self.updated_millis = updated_millis

    @property
    def allocatable_pods(self):
        """Gets the allocatable_pods of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501


        :return: The allocatable_pods of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :rtype: int
        """
        return self._allocatable_pods

    @allocatable_pods.setter
    def allocatable_pods(self, allocatable_pods):
        """Sets the allocatable_pods of this Cmxapiresourcesk8sclustersNodes.


        :param allocatable_pods: The allocatable_pods of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :type: int
        """

        self._allocatable_pods = allocatable_pods

    @property
    def cpu_cores(self):
        """Gets the cpu_cores of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501


        :return: The cpu_cores of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :rtype: float
        """
        return self._cpu_cores

    @cpu_cores.setter
    def cpu_cores(self, cpu_cores):
        """Sets the cpu_cores of this Cmxapiresourcesk8sclustersNodes.


        :param cpu_cores: The cpu_cores of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :type: float
        """

        self._cpu_cores = cpu_cores

    @property
    def cpu_usage(self):
        """Gets the cpu_usage of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501


        :return: The cpu_usage of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :rtype: float
        """
        return self._cpu_usage

    @cpu_usage.setter
    def cpu_usage(self, cpu_usage):
        """Sets the cpu_usage of this Cmxapiresourcesk8sclustersNodes.


        :param cpu_usage: The cpu_usage of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :type: float
        """

        self._cpu_usage = cpu_usage

    @property
    def created_millis(self):
        """Gets the created_millis of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501


        :return: The created_millis of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :rtype: int
        """
        return self._created_millis

    @created_millis.setter
    def created_millis(self, created_millis):
        """Sets the created_millis of this Cmxapiresourcesk8sclustersNodes.


        :param created_millis: The created_millis of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :type: int
        """

        self._created_millis = created_millis

    @property
    def description(self):
        """Gets the description of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501


        :return: The description of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this Cmxapiresourcesk8sclustersNodes.


        :param description: The description of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def id(self):
        """Gets the id of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501


        :return: The id of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this Cmxapiresourcesk8sclustersNodes.


        :param id: The id of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def memory_usage(self):
        """Gets the memory_usage of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501


        :return: The memory_usage of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :rtype: float
        """
        return self._memory_usage

    @memory_usage.setter
    def memory_usage(self, memory_usage):
        """Sets the memory_usage of this Cmxapiresourcesk8sclustersNodes.


        :param memory_usage: The memory_usage of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :type: float
        """

        self._memory_usage = memory_usage

    @property
    def name(self):
        """Gets the name of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501


        :return: The name of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this Cmxapiresourcesk8sclustersNodes.


        :param name: The name of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def org_id(self):
        """Gets the org_id of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501


        :return: The org_id of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :rtype: str
        """
        return self._org_id

    @org_id.setter
    def org_id(self, org_id):
        """Sets the org_id of this Cmxapiresourcesk8sclustersNodes.


        :param org_id: The org_id of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :type: str
        """

        self._org_id = org_id

    @property
    def total_memory(self):
        """Gets the total_memory of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501


        :return: The total_memory of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :rtype: float
        """
        return self._total_memory

    @total_memory.setter
    def total_memory(self, total_memory):
        """Sets the total_memory of this Cmxapiresourcesk8sclustersNodes.


        :param total_memory: The total_memory of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :type: float
        """

        self._total_memory = total_memory

    @property
    def updated_millis(self):
        """Gets the updated_millis of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501


        :return: The updated_millis of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :rtype: int
        """
        return self._updated_millis

    @updated_millis.setter
    def updated_millis(self, updated_millis):
        """Sets the updated_millis of this Cmxapiresourcesk8sclustersNodes.


        :param updated_millis: The updated_millis of this Cmxapiresourcesk8sclustersNodes.  # noqa: E501
        :type: int
        """

        self._updated_millis = updated_millis

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Cmxapiresourcesk8sclustersNodes, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Cmxapiresourcesk8sclustersNodes):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
