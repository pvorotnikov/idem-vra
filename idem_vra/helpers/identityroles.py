from pop.contract import ContractedContext


# Caching
CACHE = {}


def get_enforced_state(type: str, key: str = None):
    global CACHE

    if type in CACHE:
        if key is None:
            return ""
        elif key in CACHE[type]:
            return CACHE[type][key]
        else:
            return None
    else:
        return None


def cache_enforced_state(type: str, key: str, val: any):
    if type not in CACHE:
        CACHE[type] = {}
    CACHE[type][key] = val


def post_present(hub, ctx: ContractedContext, type: str):
    keys = ctx.cache.keys()
    for key in keys:
        val = ctx.cache[key]
        cache_enforced_state(type, key, val)


# Group Roles update management
def prepare_group_roles_payload(orgRoleNames: list = [], svcRoleNames: list = []):

    # Prepare the group roles update payload
    if len(orgRoleNames) > 0:
        org_role_payload = [
            {
                "name": orgRoleNames[0],
                "id": get_enforced_state("org_roles", orgRoleNames[0]),
            }
        ]
    else:
        org_role_payload = []

    # Prepare the group service roles update payload
    group_svc_payload = []
    for svc_role_name in svcRoleNames:
        group_svc_payload.append(
            {
                "rolesToAdd": [
                    {
                        "id": get_enforced_state("svc_roles", svc_role_name),
                        "name": svc_role_name,
                    }
                ],
                "rolesToRemove": [],
                "serviceDefinitionId": get_enforced_state("svc_roles", svc_role_name),
            }
        )

    return org_role_payload, group_svc_payload


# User Roles update management
def prepare_user_roles_payload(
    hub,
    org_role_names: list = [],
    svc_role_names: list = [],
    user: str = "unknown",
    action: str = "add",
):

    if action == "remove":
        key1 = "roleNamesToAdd"
        key2 = "roleNamesToRemove"
    elif action == "add":
        key1 = "roleNamesToRemove"
        key2 = "roleNamesToAdd"
    else:
        hub.log.error("Unknown action type, please specify [add or remove]!")
        return [], []

    user_svc_payload = []
    for svc_role_name in svc_role_names:
        user_svc_payload.append(
            {
                key1: [],
                key2: [svc_role_name],
                "serviceId": get_enforced_state("svc_roles", svc_role_name),
                "serviceType": "",
            }
        )

    userList = [
        {"userName": user, "orgRolesUpdateRequest": {key1: [], key2: org_role_names}}
    ]

    return userList, user_svc_payload
