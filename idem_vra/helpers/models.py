"""
vRA sub convenience classes module.

This file has classes common across all exec and states.
"""
from typing import Any

from idem_vra.helpers import constants


class StateReturn(dict):
    """
    Convenience class used to manage POP state returns. The
    underlaying dict has four keys: 'name', 'result', 'comment'
    and 'changes'.

    Methods
    -------
    set_result(result: bool)
        Sets the 'result' key in the dict.
    """

    def __init__(
        self,
        name: str = "",
        result: bool = False,
        old: Any = None,
        new: Any = None,
        comment: str = "",
    ):
        """
        Initialize an object of this class.

        :param result: True if the state call works, False otherwise.
        :param new: For state changes, the object's state
            prior to any state change request executions.
        :param old: For state changes, the new object's state
            after any state change request executions.
        :param comment: Any relevant comments to the state execution
            such as status code, exception, or other context for the
            given result.
        """

        super().__init__(
            {
                constants.NAME: name,
                constants.RESULT: result,
                constants.COMMENT: (comment,),
                constants.OLD_STATE: old,
                constants.NEW_STATE: new,
            }
        )

    def set_result(self, result: bool):
        """
        Sets the 'result' key in the dict.

        :param result: Value to store in the 'result' key.
        """

        super().__setitem__(constants.RESULT, result)


class ExecReturn(dict):
    """
    Convenience class used to manage POP exec returns. The
    underlaying dict has three keys: 'result', 'ret' and 'comment'.
    """

    def __init__(self, result: bool = False, ret: Any = None, comment: str = ""):
        """
        Initialize an object of this class.

        :param result: True if the exec call works, False otherwise.
        :param ret: The return data from the exec module run.
        :param comment: Any relevant comments to the exec execution
            such as status code, exception, or other context for the
            given result.
        """

        super().__init__(
            {
                constants.RESULT: result,
                constants.RET: ret,
                constants.COMMENT: (comment,),
            }
        )


class FunctionReturn(dict):
    """
    Convenience class used to manage POP function returns. The
    underlaying dict has a single key: 'ret'.
    """

    def __init__(self, ret: Any = None):
        """
        Initialize an object of this class.

        :param ret: The return data from the module run.
        """

        super().__init__(
            {
                constants.RET: ret,
            }
        )
