def get_extras(hub, ctx):

    # Try to get the extras
    try:
        return hub.OPT.acct.extras or {}
    except Exception as e:
        hub.log.warning(str(e))
        return {}
