pipelines
=========

.. automodule:: idem_vra.states.vra.pipeline.pipelines
   :members:
   :undoc-members:
   :show-inheritance:
