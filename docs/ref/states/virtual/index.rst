virtual
=======

.. toctree::
   :maxdepth: 4

   fabriccomputestate
   fabricnetworkstate
   fabricvspheredatastorestate
   identityrolestate
   projectresourcetagsstate
   securitygroupstate
   vspherefabricnetworkstate
