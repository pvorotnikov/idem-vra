blueprint
=========

.. automodule:: idem_vra.states.vra.blueprint.blueprint
   :members:
   :undoc-members:
   :show-inheritance:
