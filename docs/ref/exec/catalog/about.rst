about
=====

.. automodule:: idem_vra.exec.vra.catalog.about
   :members:
   :undoc-members:
   :show-inheritance:
