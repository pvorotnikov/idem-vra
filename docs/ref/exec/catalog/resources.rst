resources
=========

.. automodule:: idem_vra.exec.vra.catalog.resources
   :members:
   :undoc-members:
   :show-inheritance:
