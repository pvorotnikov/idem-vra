secrets
=======

.. automodule:: idem_vra.exec.vra.platform.secrets
   :members:
   :undoc-members:
   :show-inheritance:
