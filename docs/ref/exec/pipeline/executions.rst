executions
==========

.. automodule:: idem_vra.exec.vra.pipeline.executions
   :members:
   :undoc-members:
   :show-inheritance:
