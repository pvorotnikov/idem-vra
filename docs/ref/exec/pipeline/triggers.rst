triggers
========

.. automodule:: idem_vra.exec.vra.pipeline.triggers
   :members:
   :undoc-members:
   :show-inheritance:
