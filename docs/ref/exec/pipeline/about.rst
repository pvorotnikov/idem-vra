about
=====

.. automodule:: idem_vra.exec.vra.pipeline.about
   :members:
   :undoc-members:
   :show-inheritance:
